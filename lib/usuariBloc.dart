import 'dart:async';
import 'usuari.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class UsuariBloc {
  List<Usuari> _usuaris = [];
  String actual_token = null;
  String server_host = "10.0.2.2";
  
  //stream controller
  StreamController<List<Usuari>> _usuariListStreamController = new BehaviorSubject();
  StreamController<Usuari> _usuariRebreDadesController = new BehaviorSubject();
  final _usuariModificarDadesController = StreamController<Usuari>();
  final _usuariFollowStreamController = StreamController<Usuari>();
  final _usuariUnfollowStreamController = StreamController<Usuari>();
  final _desiredUserStreamController = StreamController<Usuari>();

  //getters streams y sinks
  Stream <List<Usuari>> get usuariListStream => _usuariListStreamController.stream;
  StreamSink <List<Usuari>> get usuariListSink => _usuariListStreamController.sink;

  Stream <Usuari> get usuariRebreDadesStream => _usuariRebreDadesController.stream;
  StreamSink <Usuari> get usuariRebreDadesSink => _usuariRebreDadesController.sink;

  StreamSink <Usuari> get usuariModificarDades => _usuariModificarDadesController.sink;
  StreamSink <Usuari> get usuariFollow => _usuariFollowStreamController.sink;
  StreamSink <Usuari> get usuariUnfollow => _usuariUnfollowStreamController.sink;

  UsuariBloc(String token, http.Client client, Usuari usuari_actual) {
    actual_token = token;
    usuariListSink.add(_usuaris);
    _usuariModificarDadesController.stream.listen(_modificaDades);
    _usuariFollowStreamController.stream.listen(_seguirUsuari);
    _usuariUnfollowStreamController.stream.listen(_unseguirUsuari);
    GetLlistaUsuaris(client, usuari_actual);
  }

  GetLlistaUsuaris(http.Client client, Usuari actual) async {
    setSeguits(actual);
    setSeguidors(actual);

    var headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    var request = http.Request('GET', Uri.parse('http://${server_host}:8000/user/search'));
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      final resposta = await response.stream.bytesToString();
      List<dynamic> data = json.decode(resposta);
      List<Usuari> llista_users = [];
      data.forEach((usuari) {
        if(usuari['id'] != actual.id) {
          llista_users.add(Usuari.fromJson(usuari));
        }
      });
      setUsuaris(llista_users);
    }
    else {
      print(response.reasonPhrase);
    }
  }

  List<Usuari> setUsuaris(List<Usuari> usuaris) {
    _usuaris = usuaris;
    usuariListSink.add(_usuaris);
  }

  _modificaDades(Usuari usuari) async {
    usuariListSink.add(_usuaris);
    final url = Uri.parse('http://${server_host}:8000/user/');
    Map<String, String> headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'token ' + actual_token,
    };

    var request = http.MultipartRequest('PUT', url);
    request.fields.addAll({
      'username': usuari.nomUsuari,
      'dni': usuari.dni,
      'email': usuari.email,
      'city': usuari.ciutat,
    });

    if(usuari.foto != null) {
      request.files.add(await http.MultipartFile.fromPath('image', usuari.foto.path));
    }
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      _getUsuari();
    }
  }

  _getUsuari() async {
    var headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : 'token ' + actual_token,
    };
    var request = http.Request('GET', Uri.parse('http://${server_host}:8000/user'));
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();
    final resposta = await response.stream.bytesToString();
    Usuari user = Usuari.fromJson(json.decode(resposta));

    bool retornar1 = await setSeguits(user);
    bool retornar2 = await setSeguidors(user);

    if(retornar1 == true && retornar2 == true) {
      usuariRebreDadesSink.add(user);
    }
  }

  setSeguits(Usuari user) async {
    http.Client client = new http.Client();
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'token ' + actual_token,
    };
    final response = await client.get(Uri.parse('http://${server_host}:8000/user_following'),
        headers: headers);
    final parsed = jsonDecode(response.body);
    final parsed2 = parsed['body'].cast<Map<String, dynamic>>();
    user.seguits = parsed2.map<int>((row) => row['target'] as int).toList();
    user.n_seguits = parsed['count'];
    return true;
  }

  setSeguidors(Usuari user) async {
    http.Client client = new http.Client();
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'token ' + actual_token,
    };
    final response = await client.get(Uri.parse('http://${server_host}:8000/user_follower/'),
        headers: headers);
    final parsed = jsonDecode(response.body);
    final parsed2 = parsed['body'].cast<Map<String, dynamic>>();
    user.seguidors = parsed2.map<int>((row) => row['source'] as int).toList();
    user.n_seguidors = parsed['count'];
    return true;
  }

  _seguirUsuari(Usuari usuari_seguit) async {
    usuariListSink.add(_usuaris);
    final url = Uri.parse('http://${server_host}:8000/user_following/');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'token ' + actual_token,
    };

    final response = await http.post(
      url, headers: headers, body: jsonEncode(<String, dynamic>{
        'target': usuari_seguit.id,
      }),
    );

    if (response.statusCode == 201) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      print("Follow ok");
    }
  }

  _unseguirUsuari(Usuari usuari_unseguit) async {
    usuariListSink.add(_usuaris);
    final url = Uri.parse('http://${server_host}:8000/user_unfollowing/');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'token ' + actual_token,
    };

    final response = await http.delete(
      url, headers: headers, body: jsonEncode(<String, dynamic>{
      'target': usuari_unseguit.id,
    }),
    );

    if (response.statusCode == 201) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      print("Unfollow ok");
    }
  }

  void dispose() {
    _usuariListStreamController.close();
    _usuariModificarDadesController.close();
    _usuariFollowStreamController.close();
    _usuariUnfollowStreamController.close();
  }
}