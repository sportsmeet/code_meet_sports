import 'package:app_meet_sports/eventBloc.dart';
import 'package:flutter/material.dart';
import 'event.dart';
import 'homePage.dart';
import 'usuari.dart';
import 'usuariBloc.dart';
import 'sport.dart';
import 'modify_event.dart';
import 'ratingScreen.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class DetallsPage extends StatefulWidget {
  Event detallEvent;
  final EventBloc eventBloc;
  final Usuari usuariActual;
  final UsuariBloc usuariBloc;
  final Map<int, String> sportDict;
  final List<Sport> sportList;
  final List<Usuari> usuariList;
  final String token;

  DetallsPage({Key key, @required this.detallEvent, this.eventBloc, this.usuariActual, this.usuariBloc, this.usuariList, this.sportDict, this.sportList, this.token}) : super(key: key);

  @override
  _DetallsPageState createState() => _DetallsPageState();
}

class _DetallsPageState extends State<DetallsPage> {
  String boto_inscripcio = "Inscriure'm";
  List<String> llistaMembres = [];

  @override
  void initState() {

    widget.detallEvent.activeSon.participantsIDRating.forEach((key, value) {
      widget.usuariList.forEach((element) {
        if(key == element.id)
          llistaMembres.add(element.nomUsuari);
      });
    });

    widget.detallEvent.rating = 0;
    if (widget.detallEvent.activeSon.participantsIDRating.isNotEmpty) {
      widget.detallEvent.activeSon.participantsIDRating.forEach((key, value) {
        if(value != null)
          widget.detallEvent.rating += value;
      });
      widget.detallEvent.rating /= widget.detallEvent.activeSon.participantsIDRating.length;
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text("Detalls event"),
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              children: [
                DataTable(
                  dataRowHeight: 60,
                  columns: const <DataColumn> [
                    DataColumn(
                      label: Text(
                        'Característiques',
                        style: TextStyle(fontStyle: FontStyle.italic),
                      ),
                    ),
                    DataColumn(
                      label: Text(
                        'Descripció',
                        style: TextStyle(fontStyle: FontStyle.italic),
                      ),
                    ),
                  ],
                  rows: <DataRow> [
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text('Esport')),
                        DataCell(Text('${widget.detallEvent.sportType.sportName}')),
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text('Codi event')),
                        DataCell(Text('${widget.detallEvent.codi}')),
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text('Autor')),
                        DataCell(Text('${widget.detallEvent.creator.nomUsuari}')),
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text('Lloc realització')),
                        DataCell(Container(
                          width: 180,
                          child:
                            Text('${widget.detallEvent.city}, ${widget.detallEvent.direction}'),
                        )),
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text('Data i hora')),
                        DataCell(Text('${widget.detallEvent.startDate} a les ${widget.detallEvent.startTime}')),
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text('Comptador')),
                        DataCell(Text('${widget.detallEvent.activeSon.nParticipants}/${widget.detallEvent.maxParticipants}')),
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text('Puntuació')),
                        DataCell(
                            RatingBar.builder(
                              initialRating: widget.detallEvent.rating,
                              minRating: 0,
                              maxRating: 5,
                              allowHalfRating: true,
                              ignoreGestures: true,
                              direction: Axis.horizontal,
                              itemCount: 5,
                              itemSize: 23,
                              itemBuilder: (context, _) => Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                          )
                        ),
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text('Participants')),
                        DataCell(
                            Container(
                                width: 180,
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: llistaMembres.length,
                                  itemBuilder: (context, index) {
                                    return ListTile(
                                      leading: Icon(Icons.account_circle_sharp),
                                      title: Text('${llistaMembres[index]}'),
                                    );
                                    //),
                                  },
                                )
                            )
                        ),
                      ],
                    )
                  ]
                ),
              ]
            ),

            Container(
              child: widget.usuariActual.id == widget.detallEvent.creator.id ? Container(
                margin: const EdgeInsets.only(top: 30.0),
                child: Row(
                  children: [
                    SizedBox(width: 100),
                    ElevatedButton(
                      child: Text('Modificar'),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ModifyEventScreen(eventModified: widget.detallEvent, eventBloc: widget.eventBloc, usuariActual: widget.usuariActual,
                                sportDict: widget.sportDict, sportList: widget.sportList, token: widget.token),
                          ),
                        );
                      },
                    ),
                    SizedBox(width: 20),
                    ElevatedButton(
                      child: Text('Eliminar'),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(Colors.red),
                      ),
                      onPressed: () {
                        widget.eventBloc.eliminaEvent(widget.detallEvent);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => HomePage(usuari_actual: widget.usuariActual, act_token: widget.token,)
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ) : Container()
            ),
          ],
        ),
      ),
    );
  }
}

class OldDetallsPage extends StatefulWidget {
  Event detallEvent;
  final EventBloc eventBloc;
  final Usuari usuariActual;
  final UsuariBloc usuariBloc;
  final Map<int, String> sportDict;
  final List<Sport> sportList;
  final List<Usuari> usuariList;
  final String token;

  OldDetallsPage({Key key, @required this.detallEvent, this.eventBloc, this.usuariActual, this.usuariBloc,
    this.usuariList, this.sportDict, this.sportList, this.token}) : super(key: key);

  @override
  _OldDetallsPageState createState() => _OldDetallsPageState();
}

class _OldDetallsPageState extends State<OldDetallsPage> {
  String boto_inscripcio = "Inscriure'm";
  List<String> llistaMembres = [];

  @override
  void initState() {
    widget.eventBloc.rebrePuntuacioSink.add(widget.detallEvent);
    widget.eventBloc.rebrePuntuacioStream.listen((onData){
      if(mounted) {
        setState(() {
          widget.detallEvent = onData;
          widget.detallEvent.rating = 0;
          if (widget.detallEvent.inactiveSon.participantsIDRating.isNotEmpty) {
            widget.detallEvent.inactiveSon.participantsIDRating.forEach((key, value) {
              if(value != null)
                widget.detallEvent.rating += value;
            });
            widget.detallEvent.rating /= widget.detallEvent.inactiveSon.participantsIDRating.length;
          }
        });
      }
    });

    widget.detallEvent.inactiveSon.participantsIDRating.forEach((key, value) {
      widget.usuariList.forEach((element) {
        if(key == element.id)
          llistaMembres.add(element.nomUsuari);
      });
    });

    widget.detallEvent.rating = 0;
    if (widget.detallEvent.inactiveSon.participantsIDRating.isNotEmpty) {
      widget.detallEvent.inactiveSon.participantsIDRating.forEach((key, value) {
        if(value != null)
          widget.detallEvent.rating += value;
      });
      widget.detallEvent.rating /= widget.detallEvent.inactiveSon.participantsIDRating.length;
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text("Detalls event"),
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
                children: [
                  DataTable(
                      dataRowHeight: 60,
                      columns: const <DataColumn> [
                        DataColumn(
                          label: Text(
                            'Característiques',
                            style: TextStyle(fontStyle: FontStyle.italic),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            'Descripció',
                            style: TextStyle(fontStyle: FontStyle.italic),
                          ),
                        ),
                      ],
                      rows: <DataRow> [
                        DataRow(
                          cells: <DataCell>[
                            DataCell(Text('Esport')),
                            DataCell(Text('${widget.detallEvent.sportType.sportName}')),
                          ],
                        ),
                        DataRow(
                          cells: <DataCell>[
                            DataCell(Text('Codi event')),
                            DataCell(Text('${widget.detallEvent.codi}')),
                          ],
                        ),
                        DataRow(
                          cells: <DataCell>[
                            DataCell(Text('Autor')),
                            DataCell(Text('${widget.detallEvent.creator.nomUsuari}')),
                          ],
                        ),
                        DataRow(
                          cells: <DataCell>[
                            DataCell(Text('Lloc realització')),
                            DataCell(Container(
                              width: 180,
                              child:
                              Text('${widget.detallEvent.city}, ${widget.detallEvent.direction}'),
                            )),
                          ],
                        ),
                        DataRow(
                          cells: <DataCell>[
                            DataCell(Text('Data i hora')),
                            DataCell(Text('${widget.detallEvent.startDate} a les ${widget.detallEvent.startTime}')),
                          ],
                        ),
                        DataRow(
                          cells: <DataCell>[
                            DataCell(Text('Comptador')),
                            DataCell(Text('${widget.detallEvent.inactiveSon.nParticipants}/${widget.detallEvent.maxParticipants}')),
                          ],
                        ),
                        DataRow(
                          cells: <DataCell>[
                            DataCell(Text('Puntuació')),
                            DataCell(
                                RatingBar.builder(
                                  initialRating: widget.detallEvent.rating,
                                  minRating: 0,
                                  maxRating: 5,
                                  allowHalfRating: true,
                                  ignoreGestures: true,
                                  direction: Axis.horizontal,
                                  itemCount: 5,
                                  itemSize: 23,
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: Colors.amber,
                                  ),
                                )
                            ),
                          ],
                        ),
                        DataRow(
                          cells: <DataCell>[
                            DataCell(Text('Participants')),
                            DataCell(
                                Container(
                                    width: 180,
                                    child: ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: llistaMembres.length,
                                      itemBuilder: (context, index) {
                                        return ListTile(
                                          leading: Icon(Icons.account_circle_sharp),
                                          title: Text('${llistaMembres[index]}'),
                                        );
                                        //),
                                      },
                                    )
                                )
                            ),
                          ],
                        )
                      ]
                  ),
                ]
            ),
            Container(
              margin: const EdgeInsets.only(top: 30.0),
              child: llistaMembres.contains(widget.usuariActual.nomUsuari) && widget.usuariActual != widget.detallEvent.creator  ? ElevatedButton(
                child: Text('Puntuar'),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => RatingScreen(detallEvent: widget.detallEvent, eventBloc: widget.eventBloc, usuariActual: widget.usuariActual, usuariList: widget.usuariList,
                        sportDict: widget.sportDict, sportList: widget.sportList, token: widget.token,),
                    ),
                  );
                },
              ) : Container(),
            ),
          ],
        ),
      ),
    );
  }
}