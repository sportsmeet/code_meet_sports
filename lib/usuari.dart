import 'dart:io';

class Usuari {
  int id;
  String dni;
  String email;
  String nomUsuari;
  String psw;
  String ciutat;
  String base_path_foto = "http://10.0.2.2:8000";
  String foto_path;
  File foto;
  int n_seguidors;
  int n_seguits;
  List<int> seguidors;
  List<int> seguits;

  Usuari({this.id, this.dni, this.email, this.nomUsuari, this.psw, this.ciutat, this.n_seguidors, this.foto_path});

  factory Usuari.fromJson(Map<String, dynamic> json) {
    return Usuari(
      id: json['id'] as int,
      dni: json['dni'] as String,
      email: json['email'] as String,
      nomUsuari: json['username'] as String,
      psw: json['password'] as String,
      ciutat: json['city'] as String,
      foto_path: json['image'] as String,
      n_seguidors: json['n_followers'] as int,
    );
  }

}