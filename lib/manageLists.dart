import 'package:flutter/material.dart';
import 'event.dart';
import 'eventBloc.dart';
import 'detallsPage.dart';
import 'modify_event.dart';
import 'package:app_meet_sports/usuari.dart';
import 'usuari.dart';
import 'usuariBloc.dart';
import 'sport.dart';
import 'edit_profile.dart';

class ManageEventList extends StatefulWidget {
  final Event event_llista;
  final EventBloc event_bloc;
  final Usuari usuari_actual;
  final UsuariBloc usuari_bloc;
  final Map<int, String> sportDict;
  final List<Sport> sportList;
  final List<Usuari> usuariList;
  final String token;

  ManageEventList({this.event_llista, this.event_bloc, this.usuari_actual,this.usuari_bloc, this.usuariList, this.sportDict, this.sportList, this.token});

  @override
  _ManageEventListState createState() => new _ManageEventListState();
}

class _ManageEventListState extends State<ManageEventList> {
  void dispose() {
    super.dispose();
  }

  String alertText;

  @override
  Widget build(BuildContext context) {
    String boto_inscripcio = "Inscriure'm";
    if(widget.event_llista.activeSon.participantsIDRating.containsKey(widget.usuari_actual.id)) {
      boto_inscripcio = "Abandonar";
    }
    return Card(
      elevation: 5.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
              padding: EdgeInsets.all(0.0),
              margin: EdgeInsets.only(left: 10.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(30.0),
                child: Image(
                  image: NetworkImage(widget.event_llista.host + widget.event_llista.sportType.img),
                  width: 120,
                  height: 120,
                  fit: BoxFit.contain,
                ),
              )
          ),
          Container(
              constraints: BoxConstraints(minWidth: 240),
              padding: EdgeInsets.all(10.0),
              margin: EdgeInsets.only(left: 10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "${widget.event_llista.title}",
                    style: TextStyle(fontSize:25.0),
                  ),
                  Text(
                    "Creador: ${widget.event_llista.creator.nomUsuari}",
                    style: TextStyle(fontSize:17.5),
                  ),
                  Text(
                    "Localització: ${widget.event_llista.city}",
                    style: TextStyle(fontSize:17.5),
                  ),
                  Text(
                    "Participants: ${widget.event_llista.activeSon.nParticipants}/${widget.event_llista.maxParticipants}",
                    style: TextStyle(fontSize:17.5),
                  ),
                  Row(
                    children: <Widget>[
                      Container (
                        child: widget.usuari_actual.id == widget.event_llista.creator.id ?
                        ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(Colors.blueGrey),
                          ),
                          child: Text("Modificar"),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ModifyEventScreen(eventModified: widget.event_llista, eventBloc: widget.event_bloc, usuariActual: widget.usuari_actual,
                                    sportDict: widget.sportDict, sportList: widget.sportList, token: widget.token),
                              ),
                            );
                          },
                        ) :
                        ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(Colors.transparent),
                          ),
                          child: Text(boto_inscripcio),
                          onPressed: () {
                            if(boto_inscripcio=="Inscriure'm") {
                              //Si l'event no es troba ple
                              if(widget.event_llista.activeSon.nParticipants < widget.event_llista.maxParticipants) {
                                widget.event_llista.activeSon.nParticipants += 1;
                                widget.event_bloc.eventModificarDades.add(widget.event_llista,);
                                widget.event_bloc.inscripcioEvent(widget.event_llista, widget.usuari_actual);
                                widget.event_llista.activeSon.participantsIDRating[widget.usuari_actual.id] = 0;
                                widget.event_llista.activeSon.participantsList.add(widget.usuari_actual.id);
                              }
                              else {
                                widget.event_llista.activeSon.participantsList.remove(widget.usuari_actual.id);
                              }
                            } else {
                              widget.event_llista.activeSon.nParticipants -= 1;
                              widget.event_bloc.eventModificarDades.add(widget.event_llista);
                              widget.event_bloc.abandonarEvent(widget.event_llista, widget.usuari_actual);
                              widget.event_llista.activeSon.participantsIDRating.remove(widget.usuari_actual.id);
                              alertText = "Has abandonat l'event correctament.";
                            };
                          },
                        ),
                      ),
                      SizedBox(width: 20),
                      ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(Colors.transparent),
                        ),
                        child: Text("Detalls"),
                        onPressed: (){
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => DetallsPage(detallEvent: widget.event_llista, eventBloc: widget.event_bloc, usuariActual: widget.usuari_actual,
                                  usuariBloc: widget.usuari_bloc, usuariList: widget.usuariList, sportDict: widget.sportDict, sportList: widget.sportList, token: widget.token),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ],
              )
          ),
        ],
      ),
    );
  }
}

class ManageOldEventList extends StatefulWidget {
  final Event event_llista;
  final EventBloc event_bloc;
  final Usuari usuari_actual;
  final UsuariBloc usuari_bloc;
  final Map<int, String> sportDict;
  final List<Sport> sportList;
  final List<Usuari> usuariList;
  final String token;

  ManageOldEventList({this.event_llista, this.event_bloc, this.usuari_actual,this.usuari_bloc, this.usuariList,
    this.sportDict, this.sportList, this.token});

  @override
  _ManageOldEventListState createState() => new _ManageOldEventListState();
}

class _ManageOldEventListState extends State<ManageOldEventList> {
  void dispose() {
    super.dispose();
  }

  String alertText;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
              padding: EdgeInsets.all(0.0),
              margin: EdgeInsets.only(left: 10.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(30.0),
                child: Image(
                  image: NetworkImage(widget.event_llista.host + widget.event_llista.sportType.img),
                  width: 120,
                  height: 120,
                  fit: BoxFit.contain,
                ),
              )
          ),
          Container(
              constraints: BoxConstraints(minWidth: 240),
              padding: EdgeInsets.all(10.0),
              margin: EdgeInsets.only(left: 10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "${widget.event_llista.title}",
                    style: TextStyle(fontSize:25.0),
                  ),
                  Text(
                    "Creador: ${widget.event_llista.creator.nomUsuari}",
                    style: TextStyle(fontSize:17.5),
                  ),
                  Text(
                    "Localització: ${widget.event_llista.city}",
                    style: TextStyle(fontSize:17.5),
                  ),
                  Text(
                    "Participants: ${widget.event_llista.inactiveSon.nParticipants}/${widget.event_llista.maxParticipants}",
                    style: TextStyle(fontSize:17.5),
                  ),
                  Row(
                    children: <Widget>[
                      ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(Colors.transparent),
                        ),
                        child: Text("Detalls"),
                        onPressed: (){
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => OldDetallsPage(detallEvent: widget.event_llista, eventBloc: widget.event_bloc, usuariActual: widget.usuari_actual,
                                  usuariBloc: widget.usuari_bloc, usuariList: widget.usuariList, sportDict: widget.sportDict, sportList: widget.sportList, token: widget.token),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ],
              )
          ),
        ],
      ),
    );
  }
}

class ManageUsuariList extends StatefulWidget {
  Usuari usuari_llista = null;
  Usuari usuari_actual = null;
  UsuariBloc usuari_bloc = null;
  ManageUsuariList({this.usuari_llista, this.usuari_bloc, this.usuari_actual});

  @override
  _ManageUsuariListState createState() => new _ManageUsuariListState();
}

class _ManageUsuariListState extends State<ManageUsuariList> {
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String boto_seguir = "Follow";
    if(widget.usuari_actual.seguits.contains(widget.usuari_llista.id)) {
      boto_seguir = "Unfollow";
    }
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => EditProfilePage(perfil_actual: widget.usuari_llista),
          ),
        );
      },
      child: Card(
        elevation: 5.0,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(left: 20.0),
              width: 110,
              height: 110,
              decoration: BoxDecoration(
                border: Border.all(
                    width: 2,
                    color: Theme.of(context).scaffoldBackgroundColor),
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 2,
                      blurRadius: 10,
                      color: Colors.black.withOpacity(0.1),
                      offset: Offset(0, 10))
                ],
                shape: BoxShape.circle,
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(widget.usuari_llista.foto_path),
                ),
              ),
            ),
            Container(
                constraints: BoxConstraints(minWidth: 240),
                padding: EdgeInsets.all(10.0),
                margin: EdgeInsets.only(left: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "${widget.usuari_llista.nomUsuari}",
                      style: TextStyle(fontSize:20.0),
                    ),
                    Text(
                      "Email: ${widget.usuari_llista.email}",
                      style: TextStyle(fontSize:15),
                    ),
                    Text(
                      "Residencia: ${widget.usuari_llista.ciutat}",
                      style: TextStyle(fontSize:15),
                    ),
                    Text(
                      "Followers: ${widget.usuari_llista.n_seguidors}",
                      style: TextStyle(fontSize:15),
                    ),
                    Row(
                      children: <Widget>[
                        ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(Colors.transparent),
                          ),
                          child: Text(boto_seguir),
                          onPressed: (){
                            if(boto_seguir=="Follow") {
                              widget.usuari_llista.n_seguidors += 1;
                              widget.usuari_bloc.usuariFollow.add(
                                  widget.usuari_llista
                              );
                              widget.usuari_actual.seguits.add(widget.usuari_llista.id);
                              widget.usuari_actual.n_seguits += 1;
                            } else {
                              widget.usuari_llista.n_seguidors -= 1;
                              widget.usuari_bloc.usuariUnfollow.add(
                                  widget.usuari_llista
                              );
                              widget.usuari_actual.seguits.remove(widget.usuari_llista.id);
                              widget.usuari_actual.n_seguits -= 1;
                            };
                          },
                        ),
                      ],
                    ),
                  ],
                )
            ),
          ],
        ),
      ),
    );
  }
}