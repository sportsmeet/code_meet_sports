class Sport {
  int id;
  String img;
  String sportName;
  int category;
  int teamInfo;

  Sport({this.id, this.img, this.sportName, this.category, this.teamInfo});

  factory Sport.fromJson(Map<String, dynamic> json) {
    return Sport(
      id: json['id'] as int,
      img: json['image'] as String,
      sportName: json['name'] as String,
      category: json['category'] as int,
      teamInfo: json['team_information'] as int,
  );
}
}