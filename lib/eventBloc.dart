import 'dart:async';
import 'event.dart';
import 'eventSon.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:convert';
import 'package:http/http.dart';
import 'usuari.dart';

class EventBloc {
  List<Event> _eventList = [];
  List<Event> _eventListFiltrada = [];
  List<Event> _myEventsList = [];
  List<Event> _myEventsListFiltrada = [];
  String server_host = "10.0.2.2";

  EventSon son = EventSon();
  String _token;

  //stream controller
  StreamController<List<Event>> _eventListStreamController = new BehaviorSubject();
  StreamController<Event> _rebrePuntuacioController = new BehaviorSubject();
  StreamController<List<Event>> _myEventsController = new BehaviorSubject();
  final _eventModificarDadesController = StreamController<Event>();

  //getters streams y sinks
  Stream <List<Event>> get eventListStream => _eventListStreamController.stream;
  StreamSink <List<Event>> get eventListSink => _eventListStreamController.sink;

  Stream <Event> get rebrePuntuacioStream => _rebrePuntuacioController.stream;
  StreamSink <Event> get rebrePuntuacioSink => _rebrePuntuacioController.sink;

  Stream <List<Event>> get myEventsStream => _myEventsController.stream;
  StreamSink <List<Event>> get myEventsSink => _myEventsController.sink;

  StreamSink <Event> get eventModificarDades => _eventModificarDadesController.sink;

  EventBloc(String token, Usuari usuari_actual, Client client) {
    _token = token;
    eventListSink.add(_eventList);
    _eventModificarDadesController.stream.listen(_modificaEvent);
    GetLlistaEvents(client);
  }

  Future<Response> createEvent(Event event, EventSon eventSon) async{

    final url = Uri.parse('http://${server_host}:8000/general_event/');
    Map<String, String> headers = {'Content-Type': 'application/json', 'Authorization': 'token ' + _token};

    Response response = await post(url, headers: headers, body: jsonEncode(<String, dynamic>{
      'title' : event.title,
      'max_participants' : event.maxParticipants,
      'sport' : event.sportType.id,
      'start_date' : event.startDate,
      'start_time' : event.startTime,
      'average_rating' : event.rating,
      'competitive' : event.eventType,
      'is_periodic' : event.period,
      'is_active' : event.isActive,
      'periodicity' : event.periodicity,
      'repeat_after' : event.repeatAfter,
      'city' : event.city,
      'event_place' : event.direction,
    }),);
  }

  Future<Response> modifyEvent(Event event) async {
    final url = Uri.parse('http://${server_host}:8000/general_event/');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'token ' + _token,
    };

    Response response = await put(
      url, headers: headers, body: jsonEncode(<String, dynamic>{
      'id' : event.id,
      'title': event.title,
      'sport': event.sportType.id,
      'start_date': event.startDate,
      'start_time': event.startTime,
      'competitive': event.eventType,
      'is_active': event.isActive,
      'is_periodic' : event.period,
      'city' : event.city,
      'event_place' : event.direction,
    }),);
    return response;
  }

  Future<Response> eliminaEvent(Event event) async{

    final url = Uri.parse('http://${server_host}:8000/general_event/');
    Map<String, String> headers = {'Content-Type': 'application/json', 'Authorization': 'token ' + _token};

    Response response = await delete(url, headers: headers, body: jsonEncode(<String, dynamic>{
      'id' : event.id,
    }),);
  }

  GetLlistaEvents(Client client) async {
    var headers = {
      'Authorization': 'token ' + _token,
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    final response = await client
        .get(Uri.parse('http://${server_host}:8000/general_event/search/'), headers: headers);
    final parsed = jsonDecode(response.body).cast<Map<String, dynamic>>();
    setEvents(parsed.map<Event>((json) => Event.fromJson(json)).toList());
    getLlistaParticipants(client);
  }

  setEvents(List<Event> events) {
    _eventList = events;
    eventListSink.add(_eventList);
  }

  _modificaEvent(Event event) {
    eventListSink.add(_eventList);
  }

  List<String> llistaLocalitzacio() {
    List<String> llista = ['Tots'];
    for(final val in _eventList){
      llista.add(val.city);
    }
    return llista.toSet().toList();
  }

  actualitzarFiltres(Map<String, String> filtres, Map<int, String> sportDict) {
    _eventListFiltrada = [];
    for(final val in _eventList){
      if(val.activeSon != null) {
        val.rating = 0;
        if (val.activeSon.participantsIDRating.isNotEmpty) {
          val.activeSon.participantsIDRating.forEach((key, value) {
            if(value != null)
              val.rating += value;
          });
          val.rating /= val.activeSon.participantsIDRating.length;
        }
        if((filtres['tipus'] == 'Tots' || sportDict[val.sportType.id] == filtres['tipus']) &&
            (filtres['lloc'] == 'Tots' || val.city == filtres['lloc'])) {
          if(filtres['estat'] == 'Tots') {
            if(filtres['punts'] == 'Tots') {
              _eventListFiltrada.add(val);
            } else {
              if(val.rating == double.parse(filtres['punts'])) {
                _eventListFiltrada.add(val);
              }
            }
          } else {
            if(filtres['estat'] == 'Ocupat' && val.activeSon.nParticipants == val.maxParticipants) {
              if(filtres['punts'] == 'Tots') {
                _eventListFiltrada.add(val);
              } else {
                if(val.rating == double.parse(filtres['punts'])) {
                  _eventListFiltrada.add(val);
                }
              }
            } else {
              if(filtres['estat'] == 'Disponible' && val.activeSon.nParticipants < val.maxParticipants) {
                if(filtres['punts'] == 'Tots') {
                  _eventListFiltrada.add(val);
                } else {
                  if(val.rating == double.parse(filtres['punts'])) {
                    _eventListFiltrada.add(val);
                  }
                }
              }
            }
          }
        }
      }
    }
    eventListSink.add(_eventListFiltrada);
  }

  inscripcioEvent(Event event, Usuari usuari) async {
    _myEventsList.add(event);
    _myEventsListFiltrada.add(event);
    final url = Uri.parse('http://${server_host}:8000/join-event/');
    Map<String, String> headers = {'Content-Type': 'application/json', 'Authorization': 'token ' + _token};

    Response response = await post(url, headers: headers, body: jsonEncode(<String, dynamic>{
      'event' : event.activeSon.id,
    }),);
  }

  abandonarEvent(Event event, Usuari usuari) async {
    _myEventsList.remove(event);
    _myEventsListFiltrada.remove(event);
    final url = Uri.parse('http://${server_host}:8000/join-event/');
    Map<String, String> headers = {'Content-Type': 'application/json', 'Authorization': 'token ' + _token};

    Response response = await delete(url, headers: headers, body: jsonEncode(<String, dynamic>{
      'user' : usuari.nomUsuari,
      'event' : event.activeSon.id,
    }),);

  }

  puntuarEvent(Event event, Usuari usuari, double rating) async {
    rebrePuntuacioSink.add(event);

    final url = Uri.parse('http://${server_host}:8000/join-event/');
    Map<String, String> headers = {'Content-Type': 'application/json', 'Authorization': 'token ' + _token};

    var idInteraccio;
    var found = false;
    var i = 0;

    Response response = await get(url, headers: headers);
    var inscripcions = jsonDecode(response.body)['body'].toList();

    while((found == false) && (i < inscripcions.length)) {
      if(inscripcions[i]['event'] == event.inactiveSon.id){
        found = true;
        idInteraccio = inscripcions[i]['id'];
      }
      else
        i++;
    }

    Response response1 = await put(url, headers: headers, body: jsonEncode(<String, dynamic>{
      'id' : idInteraccio,
      'event' : event.inactiveSon.id,
      'rating' : rating,
    }),);
  }

  getLlistaParticipants(Client client) async {
    var headers = {
      'Authorization': 'token ' + _token,
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    var request = Request('GET', Uri.parse('http://${server_host}:8000/join-event/all'));
    request.headers.addAll(headers);

    StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      final resposta = await response.stream.bytesToString();
      List<dynamic> data = json.decode(resposta);

      data.forEach((element) {
        _eventList.forEach((event) {
          if(event.activeSon != null) {
            if(event.activeSon.id == element['event']) {
              event.activeSon.participantsIDRating[element['user']] = element['rating'];
              event.activeSon.nParticipants = event.activeSon.participantsIDRating.length;
            }
          } else {
            if(event.inactiveSon.id == element['event']) {
              event.inactiveSon.participantsIDRating[element['user']] = element['rating'];
              event.inactiveSon.nParticipants = event.inactiveSon.participantsIDRating.length;
            }
          }
        });
      });
      setEvents(_eventList);
    }
    else {
      print(response.reasonPhrase);
    }
  }

  SetMyEvents(Usuari usuari_actual) {
    _myEventsListFiltrada = [];
    for(final element in _eventList) {
      if(_myEventsList.contains(element) == false && element.creator.id == usuari_actual.id) {
        _myEventsList.add(element);
      }
      if(_myEventsList.contains(element) == false) {
        if(element.activeSon != null) {
          if(element.activeSon.participantsList.contains(usuari_actual.id) == true) {
            _myEventsList.add(element);
          }
        } else {
          if(element.inactiveSon.participantsList.contains(usuari_actual.id) == true) {
            _myEventsList.add(element);
          }
        }
      }
    }

    for(final myelem in _myEventsList) {
      if(_myEventsListFiltrada.contains(myelem) == false && myelem.activeSon != null) {
        if(myelem.creator.id == usuari_actual.id || myelem.activeSon.participantsList.contains(usuari_actual.id) == true) {
          _myEventsListFiltrada.add(myelem);
        }
      }
    }
    myEventsSink.add(_myEventsListFiltrada);
  }

  actualitzarMyFiltres(Map<String, String> filtres, Usuari usuari_actual) {
    _myEventsListFiltrada = [];
    for(final val in _myEventsList){
      if(filtres['estat'] == 'En curs' && val.activeSon != null) {
        if(filtres['tipus'] == 'Tots') {
          _myEventsListFiltrada.add(val);
        } else {
          if(filtres['tipus'] == "Creats" && val.creator.id == usuari_actual.id) {
            _myEventsListFiltrada.add(val);
          } else {
            if(filtres['tipus'] == "Inscrits" && val.activeSon.participantsList.contains(usuari_actual.id) == true
            && val.creator.id != usuari_actual.id) {
              _myEventsListFiltrada.add(val);
            }
          }
        }
      } else {
        if((filtres['estat'] == 'Finalitzats' && val.inactiveSon != null)) {
          if(filtres['tipus'] == 'Tots') {
            _myEventsListFiltrada.add(val);
          } else {
            if(filtres['tipus'] == "Creats" && val.creator.id == usuari_actual.id) {
              _myEventsListFiltrada.add(val);
            } else {
              if(filtres['tipus'] == "Inscrits" && val.inactiveSon.participantsList.contains(usuari_actual.id) == true
                  && val.creator.id != usuari_actual.id) {
                _myEventsListFiltrada.add(val);
              }
            }
          }
        }
      }
    }
    myEventsSink.add(_myEventsListFiltrada);
  }

  void dispose() {
    _eventModificarDadesController.close();
    _eventListStreamController.close();
  }
}