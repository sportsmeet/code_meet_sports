import 'dart:convert';
import 'dart:async';
import 'package:app_meet_sports/usuari.dart';
import 'package:flutter/material.dart';
import 'homePage.dart';
import 'loginPage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class Loading extends StatefulWidget {
  @override
  _LoadingState createState() => _LoadingState();

}

class _LoadingState extends State<Loading> {

  SharedPreferences preferences;
  String _token="";
  @override
  void initState()  {

    //this._token= as String;
    super.initState();
    _getPreferences();

    //routing();


  }

  Future _getPreferences()  async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      this._token=pref.getString("token") ?? "";
    });
    if(this._token==""){
      routing();
    }
    else {
      goToHomePage();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Spacer(),
              Center(
                  child: Image.asset("assets/imatges/meetSports.png")
              ),
              Spacer(),
              CircularProgressIndicator(),
              SizedBox(height: 200,)
            ],
          ),
        )

    );
  }

  void goToHomePage() async {
    var headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : 'token ' + _token,
    };
    var request = http.Request('GET', Uri.parse('http://10.0.2.2:8000/user'));
    request.headers.addAll(headers);
    http.StreamedResponse response2 = await request.send();
    final resposta = await response2.stream.bytesToString();
    Usuari user_act = Usuari.fromJson(json.decode(resposta));

    Navigator.pushAndRemoveUntil(context,
        MaterialPageRoute(builder: (_) => HomePage(usuari_actual: user_act, act_token: _token,)), (route) => false);
  }

  void routing() {
    if(this._token==""){
      Timer(
        Duration(milliseconds: 4000),
            ()=> Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => LogIn(),
          ),
        ),
      );
    }else {
      goToHomePage();
    }
  }


}
