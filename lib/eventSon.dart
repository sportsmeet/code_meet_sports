
class EventSon {
  int id;
  int eventParent;
  String finalScore;
  int nParticipants;
  bool isActive;

  Map<int, double> participantsIDRating = {};
  List<dynamic> participantsList = [];

  EventSon({this.id, this.eventParent, this.finalScore, this.isActive, this.participantsList, this.nParticipants});

  factory EventSon.fromJson(Map<dynamic, dynamic> json) {
    return  EventSon(
      id: json['id'] as int,
      eventParent: json['event_parent'] as int,
      finalScore: json['final_score'] as String,
      isActive: json['is_active'] as bool,
      participantsList: json['user_event_relation'] as List<dynamic>,
      nParticipants: 0,
    );
  }
}