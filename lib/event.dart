import 'usuari.dart';
import 'eventSon.dart';
import 'sport.dart';

class Event {
  int id;
  String title;
  Usuari creator;
  String description;
  Sport sportType;
  String startDate;
  String startTime;
  String city;
  String direction;
  double rating;
  bool eventType;
  String codi;

  bool period;
  String periodicity;
  int repeatAfter;
  bool isActive;

  String host = "http://10.0.2.2:8000";

  int maxParticipants;
  EventSon activeSon;
  EventSon inactiveSon;

  List<EventSon> _eventSonList = [];

  Event({this.id, this.codi, this.title, this.creator, this.maxParticipants, this.sportType, this.startDate, this.startTime,
    this.city, this.direction, this.rating, this.eventType, this.period, this.isActive, this.activeSon, this.inactiveSon});

  factory Event.fromJson(Map<String, dynamic> json) {
    List<dynamic> user = json['created_by_instance'].toList();
    Map<String, dynamic> jsonUser = user[0];

    List<dynamic> sport = json['sport_instance'].toList();
    Map<String, dynamic> jsonSport = sport[0];

    Map<String, dynamic> jsonActiveSon = null;
    Map<String, dynamic> jsonInactiveSon = null;
    List<String> time = [];

    if(json['active_event'].isEmpty == false) {
      List<dynamic> son = json['active_event'].toList();
      jsonActiveSon = son[0];
      time = jsonActiveSon['start_time'].split(":");
    }

    if(json['inactive_event'].isEmpty == false){
      List<dynamic> inactiveSons = json['inactive_event'].toList();
      jsonInactiveSon = inactiveSons[0];
      time = jsonInactiveSon['start_time'].split(":");
    }

    return Event(
      id: json['id'] as int,
      codi: json['code_event'] as String,
      title: json['title'] as String,
      creator: Usuari.fromJson(jsonUser),
      maxParticipants: json['max_participants'] as int,
      sportType: Sport.fromJson(jsonSport),
      startTime: time != [] ? "${time[0]}:${time[1]}" : "",
      startDate: jsonInactiveSon == null? jsonActiveSon['start_date'] as String : jsonInactiveSon['start_date'] as String,
      city: json['city'] as String,
      direction: json['event_place'] as String,
      rating: json['average_rating'] as double,
      eventType: json['competitive'] as bool,
      period: json['is_periodic'] as bool,
      isActive: json['is_active'] as bool,
      activeSon: jsonActiveSon != null ? EventSon.fromJson(jsonActiveSon) : null,
      inactiveSon: jsonInactiveSon != null ? EventSon.fromJson(jsonInactiveSon) : null,
    );

  }

  List<EventSon> get eventSonList => this._eventSonList;

  set eventSonList(List<EventSon> eventSonList) {
    this._eventSonList = eventSonList;
  }
}