import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'homePage.dart';
import 'package:app_meet_sports/usuari.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  

  final TextEditingController _password = TextEditingController();
  final TextEditingController _confirm_password = TextEditingController();
  final TextEditingController _username = TextEditingController();
  final TextEditingController _ciutat = TextEditingController();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _dni = TextEditingController();
  String my_token="";
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Scaffold(

        appBar: AppBar(
          title: Text("MeetSports"),
          automaticallyImplyLeading: true,
          leading: IconButton(icon: Icon(Icons.arrow_back),
            onPressed:() => Navigator.pop(context,false)),
        ),
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                Text("Registre d'usuari",
                style: TextStyle(
                  fontSize: 28,
                  fontWeight: FontWeight.bold,
                  height: 2,
                ),
                ),
                Column(
                  children: [
                    SizedBox(height: 35,),
                     TextFormField(
                       decoration: InputDecoration(
                         labelText: "DNI",
                         hintText: "DNI",
                         border: OutlineInputBorder(borderRadius: BorderRadius.circular(10))
                       ),
                       controller: _dni,
                       /*validator: (value){
                         if(value.isEmpty){
                           return "Has d'omplir el camp";
                         }
                         else if (!value.isValidDNI()){
                           return "Introdueix un DNI vàlid";
                         }
                         return null;
                       },*/
                     ),
                    SizedBox(height: 25,),
                    TextFormField(
                      decoration: InputDecoration(
                          labelText: "Email",
                          hintText: "Email",
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10))
                      ),
                      controller: _email,
                      validator: (value){
                        if(value.isEmpty){
                          return "Has d'omplir el camp";
                        }
                        else if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]").hasMatch(value)){
                          return "Introdueix un e-mail vàlid";
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 25,),
                    TextFormField(
                      decoration: InputDecoration(
                          labelText: "Nom d'usuari",
                          hintText: "Nom d'usuari",
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10))
                      ),
                      controller: _username,
                      validator: (value){
                        if(value.isEmpty){
                          return "Has d'omplir el camp";
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 25,),
                    TextFormField(
                      obscureText: true,
                      controller: _password,
                      validator: (value) {
                        if(value.isEmpty){
                          return "Has d'omplir el camp";
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          labelText: "Contrasenya",
                          hintText: "Contrasenya",
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10))
                      ),
                    ),
                    SizedBox(height: 25,),
                    TextFormField(
                      obscureText: true,
                      controller: _confirm_password,
                      decoration: InputDecoration(
                          labelText: "Repetir contrasenya",
                          hintText: "Repetir contrasenya",
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10))
                      ),
                      validator: (value){
                        if(value.isEmpty || value == null){
                          return "Has d'omplir el camp";
                        }else if (value != _password.text){
                          return "Les contrasenyes han de ser iguals";
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 25,),
                    TextFormField(
                      decoration: InputDecoration(
                          labelText: "Ciutat",
                          hintText: "Ciutat",
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10))
                      ),
                      controller: _ciutat,
                    ),
                    SizedBox(height: 30,),
                    Container(
                      height: 40,
                      width: 150,
                      decoration: BoxDecoration(
                          color: Colors.blue, borderRadius: BorderRadius.circular(20)),
                      child: FlatButton(

                          onPressed: () async {
                          if(_formKey.currentState.validate()){

                            Map<String,String> data = {
                              "username": _username.text,
                              "password": _password.text,
                            };
                            final response2= await http.post(
                              Uri.parse('http://10.0.2.2:8000/register/'),
                              body: {'username' : data["username"],
                                     'password' : data["password"],
                                     'email'    : _email.text,
                                     'city'     : _ciutat.text,
                                     'dni'      : _dni.text},
                            );

                            if(response2.statusCode == 200 || response2.statusCode == 201){
                              ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Registrat correctament!")));
                              final response= await http.post(
                                Uri.parse('http://10.0.2.2:8000/api-token-auth/'),
                                body: {'username' : data["username"],
                                  'password' : data["password"]},
                              );
                              Map<String,dynamic> output = json.decode(response.body);
                              my_token=output["token"];

                              setPreferences();
                              //TODO AQUI JA GUARDARIA EL TOKEN A SHARED PREFERENCES
                              var headers = {
                                'Content-Type': 'application/x-www-form-urlencoded',
                                'Authorization' : 'token ' + my_token,
                              };
                              var request = http.Request('GET', Uri.parse('http://10.0.2.2:8000/user'));
                              request.headers.addAll(headers);
                              http.StreamedResponse response2 = await request.send();
                              final resposta = await response2.stream.bytesToString();
                              Usuari user_act = Usuari.fromJson(json.decode(resposta));

                              Navigator.pushAndRemoveUntil(context,
                                  MaterialPageRoute(builder: (_) => HomePage(usuari_actual: user_act, act_token: my_token,)), (route) => false);

                            }else{
                              ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Usuari o contrasenya incorrectes")));
                            }
                            //TODO AQUI HAIG DE REGISTRAR L'USUARI CORRECTAMENT
                            //TODO DESPRES, GUARDAR EL TOKEN DEL USUARI REGISTRAT A SHARED PREFERENCES
                            // TODO PER ULTIM, PASAR DIRECTAMENT A HOMEPAGE, PASANTLI EL TOKEN
                            //Navigator.push(context, MaterialPageRoute(builder: (_) => LogIn()));
                          }
                          else{
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Has d'omplir el formulari correctament")));
                          }
                      },
                          child: Text(
                            'Registrar-se',
                            style: TextStyle(color: Colors.white, fontSize: 15),
                          )),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void setPreferences() async {
    final pref = await SharedPreferences.getInstance();
    setState(() {
      pref.setString("token", my_token);
    });
  }
}
