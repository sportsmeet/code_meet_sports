import 'package:shared_preferences/shared_preferences.dart';
import 'usuari.dart';
import 'usuariBloc.dart';
import 'eventBloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'manageLists.dart';
import 'package:app_meet_sports/usuari.dart';
import 'sport.dart';

class EditProfilePage extends StatefulWidget {
  Usuari perfil_actual;
  UsuariBloc usuari_bloc;
  EventBloc event_bloc;
  List<Sport> sportList;
  Map<int, String> sportDict;
  final String act_token;
  final user_list;

  EditProfilePage({Key key, @required this.perfil_actual, this.usuari_bloc, this.event_bloc,
    this.sportDict, this.sportList, this.act_token, this.user_list})
      : super(key: key);

  @override
  _EditProfilePageState createState() => _EditProfilePageState();

  @override
  void dispose() {
    usuari_bloc.dispose();
  }
}

class _EditProfilePageState extends State<EditProfilePage> {
  String control_email = "";
  String control_nom = "";
  String control_dni = "";
  String control_ciutat = "";
  File control_imageFile = null;

  String _mytoken;
  void initState(){
    super.initState();
    _getPreferences();
  }

  Future _getPreferences()  async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      this._mytoken=pref.getString("token") ;
    });
  }

  /// Get from gallery
  Future _getFromGallery() async {
    PickedFile pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    setState(() {
      if (pickedFile != null) {
        control_imageFile = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return widget.act_token == null? Scaffold(
        appBar: AppBar(
          title: Text('Perfil'),
        ),
        body: Center(
          child:
          Container(
            padding: EdgeInsets.only(left: 16, top: 20, right: 16),
            child: Column(
              children: [
                Text(
                  "Perfil usuari",
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.w500),
                ),
                SizedBox(
                  height: 15,
                ),
                Center(
                  child: Stack(
                    children: [
                      Container(
                        width: 130,
                        height: 130,
                        decoration: BoxDecoration(
                          border: Border.all(
                              width: 4,
                              color: Theme.of(context).scaffoldBackgroundColor),
                          boxShadow: [
                            BoxShadow(
                                spreadRadius: 2,
                                blurRadius: 10,
                                color: Colors.black.withOpacity(0.1),
                                offset: Offset(0, 10))
                          ],
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(widget.perfil_actual.foto_path)
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    DataTable(
                        dataRowHeight: 60,
                        columns: const <DataColumn> [
                          DataColumn(
                            label: Text(
                              '',
                              style: TextStyle(fontStyle: FontStyle.italic),
                            ),
                          ),
                          DataColumn(
                            label: Text(
                              '',
                              style: TextStyle(fontStyle: FontStyle.italic),
                            ),
                          ),
                        ],
                        rows: <DataRow> [
                          DataRow(
                            cells: <DataCell>[
                              DataCell(Text('Nom')),
                              DataCell(Text('${widget.perfil_actual.nomUsuari}')),
                            ],
                          ),
                          DataRow(
                            cells: <DataCell>[
                              DataCell(Text('Email')),
                              DataCell(Text('${widget.perfil_actual.email}')),
                            ],
                          ),
                          DataRow(
                            cells: <DataCell>[
                              DataCell(Text('Ciutat')),
                              DataCell(Text('${widget.perfil_actual.ciutat}')),
                            ],
                          ),
                          DataRow(
                            cells: <DataCell>[
                              DataCell(Text('Nº seguidors')),
                              DataCell(Text('${widget.perfil_actual.n_seguidors}')),
                            ],
                          ),
                        ]
                    ),
                  ],
                ),
                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 115, bottom: 20, top: 10.00),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(20),
                            ),),
                            onPressed: () async {
                              var headers = {
                                'Authorization': 'token ' + '${_mytoken}',
                                'Content-Type': 'application/x-www-form-urlencoded'
                              };
                              var request = http.Request('POST', Uri.parse('http://10.0.2.2:8000/report/'));
                              request.bodyFields = {
                                'target': '${widget.perfil_actual.id}'
                              };
                              request.headers.addAll(headers);

                              http.StreamedResponse response = await request.send();


                              if(response.statusCode == 200 || response.statusCode == 201) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(content: Text(
                                        "L'usuari ${widget.perfil_actual.nomUsuari} ha estat reportat.")));
                              }
                              },
                            child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "REPORTAR",
                                  style: TextStyle(
                                      fontSize: 14,
                                      letterSpacing: 2.2,
                                      color: Colors.white),
                                )
                            ),
                          )
                        ],
                      ),
                    ),
                  ]
                )
              ],
            ),
          ),
        ),
      ) :
    DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
             title: Text("Perfil"),
              bottom: TabBar(
                tabs: [
                  Tab(icon: Image.asset("assets/imatges/edit.png", width: 30.00,height: 30.00)),
                  Tab(icon: Image.asset("assets/imatges/follower.png", width: 30.00,height: 30.00)),
                  Tab(icon: Image.asset("assets/imatges/friends.png", width: 35.00,height: 35.00)),
                ],
              ),
          ),
          body: TabBarView(
            children: [
              StreamBuilder<Usuari>(
                  stream: widget.usuari_bloc.usuariRebreDadesStream,
                  builder: (BuildContext context, AsyncSnapshot<Usuari> snapshot) {
                    if(snapshot.data != null) {
                      widget.perfil_actual = snapshot.data;
                    }
                    return new Container(
                      padding: EdgeInsets.only(left: 16, top: 10, right: 16),
                      child: GestureDetector(
                        onTap: () {
                          FocusScope.of(context).unfocus();
                        },
                        child: ListView(
                          children: [
                            Text(
                              "Editar Perfil",
                              style: TextStyle(fontSize: 25, fontWeight: FontWeight.w500),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Center(
                              child: Stack(
                                children: [
                                  Container(
                                    width: 130,
                                    height: 130,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          width: 4,
                                          color: Theme.of(context).scaffoldBackgroundColor),
                                      boxShadow: [
                                        BoxShadow(
                                            spreadRadius: 2,
                                            blurRadius: 10,
                                            color: Colors.black.withOpacity(0.1),
                                            offset: Offset(0, 10))
                                      ],
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: control_imageFile == null ? NetworkImage(widget.perfil_actual.foto_path)
                                              : FileImage(control_imageFile)
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                      bottom: 0,
                                      right: 0,
                                      child: Container(
                                        height: 40,
                                        width: 40,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          border: Border.all(
                                            width: 4,
                                            color: Theme.of(context).scaffoldBackgroundColor,
                                          ),
                                          color: Colors.green,
                                        ),
                                        child: IconButton(
                                          padding: EdgeInsets.only(right: 1.0),
                                          icon: new Icon(
                                            Icons.edit,
                                            color: Colors.white,
                                          ),
                                          highlightColor: Colors.pink,
                                          onPressed: () {
                                            _getFromGallery();
                                          },
                                        ),
                                      )
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 35,
                            ),
                            buildTextField("DNI", "${widget.perfil_actual.dni}", false),
                            buildTextField("Nom", "${widget.perfil_actual.nomUsuari}", false),
                            buildTextField("E-mail", "${widget.perfil_actual.email}", false),
                            buildTextField("Ciutat", "${widget.perfil_actual.ciutat}", false),
                            Row(
                              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(left: 115, bottom: 20),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      ElevatedButton(
                                        style: ElevatedButton.styleFrom(shape: new RoundedRectangleBorder(
                                          borderRadius: new BorderRadius.circular(20),
                                        ),),
                                        onPressed: () {
                                          control_dni != "" ? widget.perfil_actual.dni = control_dni : "";
                                          control_nom != "" ? widget.perfil_actual.nomUsuari = control_nom : "";
                                          control_ciutat != "" ? widget.perfil_actual.ciutat = control_ciutat : "";
                                          control_email != "" ? widget.perfil_actual.email = control_email : "";
                                          control_imageFile != null ? widget.perfil_actual.foto = control_imageFile : "";
                                          widget.usuari_bloc.usuariModificarDades.add(widget.perfil_actual);
                                        },
                                        child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Text(
                                              "GUARDAR",
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  letterSpacing: 2.2,
                                                  color: Colors.white),
                                            )
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    );
                  }
              ),
              StreamBuilder<List<Usuari>>(
                  stream: widget.usuari_bloc.usuariListStream,
                  builder: (BuildContext context, AsyncSnapshot<List<Usuari>> snapshot) {
                    return snapshot.data == null ? Container() : Container(
                      child:
                      Column(
                        children: [
                          Container(
                            padding: EdgeInsets.only(top: 10.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                    children: [
                                      Expanded(
                                        child: new Container(
                                            margin: const EdgeInsets.only(left: 10.0, right: 20.0),
                                            child: Divider(
                                              color: Colors.black,
                                              height: 36,
                                            )),
                                      ),
                                      Text(
                                        "Seguidors (${widget.perfil_actual.n_seguidors})",
                                        style: TextStyle(fontSize:18.0, fontWeight: FontWeight.bold),
                                      ),
                                      Expanded(
                                        child: new Container(
                                            margin: const EdgeInsets.only(left: 20.0, right: 10.0),
                                            child: Divider(
                                              color: Colors.black,
                                              height: 36,
                                            )),
                                      ),
                                    ]
                                )
                              ],
                            ),
                          ),
                          Expanded(
                            child: ListView.builder(
                              shrinkWrap: true,
                              itemCount: snapshot.data.length,
                              itemBuilder: (context, index) {
                                if(widget.perfil_actual.seguidors.contains(snapshot.data[index].id)) {
                                  return new ManageUsuariList(usuari_llista: snapshot.data[index], usuari_bloc: widget.usuari_bloc, usuari_actual: widget.perfil_actual,);
                                } else {
                                  return new Container();
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    );
                  }
              ),
              StreamBuilder<List<Usuari>>(
                  stream: widget.usuari_bloc.usuariListStream,
                  builder: (BuildContext context, AsyncSnapshot<List<Usuari>> snapshot) {
                    return snapshot.data == null ? Container() : Container(
                      child:
                      Column(
                        children: [
                          Container(
                            padding: EdgeInsets.only(top: 10.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                    children: [
                                      Expanded(
                                        child: new Container(
                                            margin: const EdgeInsets.only(left: 10.0, right: 20.0),
                                            child: Divider(
                                              color: Colors.black,
                                              height: 36,
                                            )),
                                      ),
                                      Text(
                                        "Chat amics",
                                        style: TextStyle(fontSize:18.0, fontWeight: FontWeight.bold),
                                      ),
                                      Expanded(
                                        child: new Container(
                                            margin: const EdgeInsets.only(left: 20.0, right: 10.0),
                                            child: Divider(
                                              color: Colors.black,
                                              height: 36,
                                            )),
                                      ),
                                    ]
                                )
                              ],
                            ),
                          ),
                          Expanded(
                            child: ListView.builder(
                              shrinkWrap: true,
                              itemCount: snapshot.data.length,
                              itemBuilder: (context, index) {
                                if(widget.perfil_actual.seguidors.contains(snapshot.data[index].id) &&
                                    widget.perfil_actual.seguits.contains(snapshot.data[index].id)) {
                                  return Card(
                                    color: Colors.grey[800],
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.only(top: 10.00, bottom: 10.00,
                                                                           left: 10.00, right: 20.00),
                                            child: Container(
                                                width: 50.0,
                                                height: 50.0,
                                                decoration: new BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    image: new DecorationImage(
                                                      fit: BoxFit.cover,
                                                      image: NetworkImage(snapshot.data[index].foto_path),
                                                    )
                                                )
                                            ),
                                          ),
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(snapshot.data[index].nomUsuari,
                                                style: TextStyle (
                                                    color: Colors.white,
                                                    fontSize: 20
                                                ),
                                              ),
                                            ],
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(left: 180.00),
                                            child:
                                              Icon(
                                                Icons.chat_outlined,
                                                color: Colors.green,
                                                size: 35.0,
                                              ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                } else {
                                  return new Container();
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    );
                  }
              ),
            ],
      ),
        ),
    );
  }

  Widget buildTextField(String labelText, String placeholder, bool isPasswordTextField) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 35.0),
      child: TextField(
        onChanged: (text) {
          labelText == "DNI" ? control_dni = text : "";
          labelText == "Nom" ? control_nom = text : "";
          labelText == "E-mail" ? control_email = text : "";
          labelText == "Ciutat" ? control_ciutat = text : "";
        },
        decoration: InputDecoration(
            suffixIcon: null,
            suffixIconConstraints: BoxConstraints(
              maxWidth: 50,
              maxHeight: 28,
            ),
            contentPadding: EdgeInsets.only(bottom: 3),
            labelText: labelText,
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: placeholder,
            hintStyle: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            )),
      ),
    );
  }
}