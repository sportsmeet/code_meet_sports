import 'package:http/http.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:async';
import 'dart:convert';
import 'sport.dart';

class SportBloc {
  List<Sport> _sportList = [];
  String _token;

  StreamController<List<Sport>> _sportStreamController = new BehaviorSubject();

  //getters streams y sinks
  Stream <List<Sport>> get sportListStream => _sportStreamController.stream;
  StreamSink <List<Sport>> get sportListSink => _sportStreamController.sink;

  SportBloc(String token) {
    _token = token;
    sportListSink.add(_sportList);
  }

  getSportsBD(Client client) async {
    var headers = {
      'Authorization': 'token ' + _token,
    };
    final response = await client
        .get(Uri.parse('http://10.0.2.2:8000/sport/search/'), headers: headers);
    final parsed = jsonDecode(response.body).cast<Map<String, dynamic>>();
    setSports(parsed.map<Sport>((json) => Sport.fromJson(json)).toList());
  }

  setSports(List<Sport> sports) {
    _sportList = sports;
    sportListSink.add(_sportList);
  }

  void dispose() {
    _sportStreamController.close();
  }

}
