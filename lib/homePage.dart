import 'package:flutter/material.dart';
import 'event.dart';
import 'manageLists.dart';
import 'eventBloc.dart';
import 'create_event.dart';
import 'loginPage.dart';
import 'meus_events.dart';
import 'package:app_meet_sports/usuari.dart';
import 'usuariBloc.dart';
import 'sport.dart';
import 'sportBloc.dart';
import 'edit_profile.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  Usuari usuari_actual;
  String act_token;

  HomePage({Key key, @required this.usuari_actual, this.act_token}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  EventBloc _eventBloc = null;
  UsuariBloc _usuariBloc = null;
  SportBloc _sportBloc = null;
  String act_tab = "";
  List<String> llista_elements = [];
  List<Sport> sportList;
  Map<int, String> sportDict = {};
  List<String> tipusSports = ['Tots'];
  List<Usuari> usuariList = [];
  String TipusDropdownVal = 'Tots';
  String EstatDropdownVal = 'Tots';
  String LlocDropdownVal = 'Tots';
  String PuntsDropdownVal = 'Tots';
  Map<String, String> filtres = {};

  @override
  void initState() {
    _usuariBloc = UsuariBloc(widget.act_token, http.Client(), widget.usuari_actual);
    _eventBloc = EventBloc(widget.act_token, widget.usuari_actual, http.Client());
    _usuariBloc.usuariRebreDadesStream.listen((onData){
      setState(() {
        widget.usuari_actual = onData;
      });
    });

    _usuariBloc.usuariListStream.listen((onData){
      setState(() {
        onData.forEach((element) {
          usuariList.add(element);
        });
      });
    });
    _sportBloc = SportBloc(widget.act_token);
    _sportBloc.getSportsBD(http.Client());
    _sportBloc.sportListStream.listen((onData){
      setState(() {
        sportList = onData;

        if(sportList.length != 0) {
          sportList.forEach((element) {
            sportDict[element.id] = element.sportName;
            tipusSports.add(element.sportName);
          });
        }
      });
    });
    filtres = {'tipus': TipusDropdownVal,'estat': EstatDropdownVal,
      'lloc': LlocDropdownVal, 'punts': PuntsDropdownVal};

    usuariList.add(widget.usuari_actual);
  }

  @override
  void dispose() {
    super.dispose();
    _eventBloc.dispose();
    _usuariBloc.dispose();
    _sportBloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Meet Sports"),
          bottom: TabBar(
            tabs: [
              Tab(icon: Image.asset("assets/imatges/events.png", width: 35.00,height: 35.00)),
              Tab(icon: Image.asset("assets/imatges/follow.png", width: 35.00,height: 35.00))
            ],
          ),
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    if(act_tab == "events") {
                      showSearch(context: context, delegate: Search(listElem: llista_elements.toSet().toList(),
                          event_bloc: _eventBloc, usuari_actual: widget.usuari_actual));
                    } else {
                      showSearch(context: context, delegate: Search(listElem: llista_elements.toSet().toList(),
                          usuari_bloc: _usuariBloc, usuari_actual: widget.usuari_actual));
                    }
                  },
                  child: Icon(
                    Icons.search,
                    size: 26.0,
                  ),
                )
            ),
          ],
        ),
        body: TabBarView(
          children: [
            StreamBuilder<List<Event>>(
                stream: _eventBloc.eventListStream,
                builder: (BuildContext context, AsyncSnapshot<List<Event>> snapshot) {
                  act_tab = "events";
                  llista_elements = [];
                  return snapshot.data == null ? Center(child: CircularProgressIndicator()) : Container(
                      child:
                      Column(
                        children: [
                          Container(
                            padding: EdgeInsets.only(left: 10.0),
                            child:
                            Row(
                              children: [
                                Text("Tipus: "),
                                SizedBox(width: 5),
                                DropdownButton<String>(
                                  value: TipusDropdownVal,
                                  icon: const Icon(Icons.arrow_downward),
                                  iconSize: 24,
                                  elevation: 16,
                                  style: const TextStyle(color: Colors.deepPurple),
                                  underline: Container(
                                    height: 2,
                                    color: Colors.deepPurpleAccent,
                                  ),
                                  onChanged: (newValue) {
                                    setState(() {
                                      if(newValue is String) {
                                        TipusDropdownVal = newValue;
                                        filtres['tipus'] = TipusDropdownVal;
                                        _eventBloc.actualitzarFiltres(filtres, sportDict);
                                      }
                                    });
                                  },
                                  items: tipusSports.map<DropdownMenuItem<String>>((String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ),
                                SizedBox(width: 20),
                                Text("Lloc: "),
                                SizedBox(width: 5),
                                DropdownButton<String>(
                                  value: LlocDropdownVal,
                                  icon: const Icon(Icons.arrow_downward),
                                  iconSize: 24,
                                  elevation: 16,
                                  style: const TextStyle(color: Colors.deepPurple),
                                  underline: Container(
                                    height: 2,
                                    color: Colors.deepPurpleAccent,
                                  ),
                                  onChanged: (newValue) {
                                    setState(() {
                                      if(newValue is String) {
                                        LlocDropdownVal = newValue;
                                        filtres['lloc'] = LlocDropdownVal;
                                        _eventBloc.actualitzarFiltres(filtres, sportDict);
                                      }
                                    });
                                  },
                                  items: _eventBloc.llistaLocalitzacio().map<DropdownMenuItem<String>>((String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 10.0),
                            child:
                            Row(
                              children: [
                                Text("Estat: "),
                                SizedBox(width: 5),
                                DropdownButton<String>(
                                  value: EstatDropdownVal,
                                  icon: const Icon(Icons.arrow_downward),
                                  iconSize: 24,
                                  elevation: 16,
                                  style: const TextStyle(color: Colors.deepPurple),
                                  underline: Container(
                                    height: 2,
                                    color: Colors.deepPurpleAccent,
                                  ),
                                  onChanged: (newValue) {
                                    setState(() {
                                      if(newValue is String) {
                                        EstatDropdownVal = newValue;
                                        filtres['estat'] = EstatDropdownVal;
                                        _eventBloc.actualitzarFiltres(filtres, sportDict);
                                      }
                                    });
                                  },
                                  items: <String>['Tots', 'Disponible', 'Ocupat']
                                      .map<DropdownMenuItem<String>>((String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ),
                                SizedBox(width: 20),
                                Text("Puntuació: "),
                                SizedBox(width: 5),
                                DropdownButton<String>(
                                  value: PuntsDropdownVal,
                                  icon: const Icon(Icons.arrow_downward),
                                  iconSize: 24,
                                  elevation: 16,
                                  style: const TextStyle(color: Colors.deepPurple),
                                  underline: Container(
                                    height: 2,
                                    color: Colors.deepPurpleAccent,
                                  ),
                                  onChanged: (newValue) {
                                    setState(() {
                                      if(newValue is String) {
                                        PuntsDropdownVal = newValue;
                                        filtres['punts'] = PuntsDropdownVal;
                                        _eventBloc.actualitzarFiltres(filtres, sportDict);
                                      }
                                    });
                                  },
                                  items: <String>['Tots', '1', '2', '3', '4', '5'].map<DropdownMenuItem<String>>((String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                              child:
                              ListView.builder(
                                itemCount: snapshot.data.length,
                                itemBuilder: (context, index) {
                                  llista_elements.add(snapshot.data[index].codi);
                                  if(snapshot.data[index].activeSon != null) {
                                    return new ManageEventList(event_llista: snapshot.data[index], event_bloc: _eventBloc, usuari_actual: widget.usuari_actual,
                                        usuari_bloc: _usuariBloc, usuariList: usuariList, sportDict: sportDict, sportList: sportList, token: widget.act_token);
                                  } else {
                                    return Container();
                                  }
                                },
                              )
                          ),
                        ],
                      )
                  );
                }
            ),
            StreamBuilder<List<Usuari>>(
                stream: _usuariBloc.usuariListStream,
                builder: (BuildContext context, AsyncSnapshot<List<Usuari>> snapshot) {
                  act_tab = "usuaris";
                  llista_elements = [];
                  return snapshot.data == null ? Center(child: CircularProgressIndicator()) : Container(
                    child:
                    Column(
                      children: [
                        Container(
                          padding: EdgeInsets.only(top: 10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                  children: [
                                    Expanded(
                                      child: new Container(
                                          margin: const EdgeInsets.only(left: 10.0, right: 20.0),
                                          child: Divider(
                                            color: Colors.black,
                                            height: 36,
                                          )),
                                    ),
                                    Text(
                                      "Seguint (${widget.usuari_actual.n_seguits})",
                                      style: TextStyle(fontSize:18.0, fontWeight: FontWeight.bold),
                                    ),
                                    Expanded(
                                      child: new Container(
                                          margin: const EdgeInsets.only(left: 20.0, right: 10.0),
                                          child: Divider(
                                            color: Colors.black,
                                            height: 36,
                                          )
                                      ),
                                    ),
                                  ]
                              )
                            ],
                          ),
                        ),
                        Expanded(
                          child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: snapshot.data.length,
                            itemBuilder: (context, index) {
                              llista_elements.add(snapshot.data[index].nomUsuari);
                              if(widget.usuari_actual.seguits.contains(snapshot.data[index].id)) {
                                return new ManageUsuariList(usuari_llista: snapshot.data[index], usuari_bloc: _usuariBloc,
                                  usuari_actual: widget.usuari_actual,);
                              } else {
                                return new Container();
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  );
                }
            ),
          ],
        ),
        drawer: Drawer(
          child: ListView(
            // Important: Remove any padding from the ListView.
            padding: EdgeInsets.zero,
            children: <Widget>[
              Container(
                height: 150.0,
                child: DrawerHeader(
                  child: widget.usuari_actual != null ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.only(bottom: 10.0),
                        width: 70,
                        height: 70,
                        decoration: BoxDecoration(
                          border: Border.all(
                              width: 2,
                              color: Theme.of(context).scaffoldBackgroundColor),
                          boxShadow: [
                            BoxShadow(
                                spreadRadius: 2,
                                blurRadius: 10,
                                color: Colors.black.withOpacity(0.1),
                                offset: Offset(0, 10))
                          ],
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(widget.usuari_actual.foto_path),
                          ),
                        ),
                      ),
                      Text('Hola, ${widget.usuari_actual.nomUsuari}',style: TextStyle(color: Colors.white, fontSize: 16)),
                    ],
                  ) : Container(),
                  decoration: BoxDecoration(
                      color: Colors.indigo
                  ),
                  margin: EdgeInsets.all(0.0),
                  padding: EdgeInsets.only(left: 15.0, right: 20.0, top: 15.0),
                ),
              ),
              ListTile(
                title: Text('Perfil', style: TextStyle(fontSize: 16)),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => EditProfilePage(perfil_actual: widget.usuari_actual,
                          usuari_bloc: _usuariBloc, event_bloc: _eventBloc, sportDict: sportDict,
                          sportList: sportList, act_token: widget.act_token, user_list: usuariList),
                    ),
                  );
                },
              ),
              ListTile(
                title: Text('Els teus events', style: TextStyle(fontSize: 16)),
                onTap: () {
                  _eventBloc.SetMyEvents(widget.usuari_actual);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MyEvents(perfil_actual: widget.usuari_actual,
                          usuari_bloc: _usuariBloc, event_bloc: _eventBloc, sportDict: sportDict,
                          sportList: sportList, act_token: widget.act_token, user_list: usuariList),
                    ),
                  );
                },
              ),
              ListTile(
                title: Text('Ajuda', style: TextStyle(fontSize: 16)),
                onTap: () {
                  // Update the state of the app.
                  // ...
                },
              ),
              ListTile(
                title: Text('Idioma', style: TextStyle(fontSize: 16)),
                onTap: () {
                  // Update the state of the app.
                  // ...
                },
              ),
              ListTile(
                title: Text('Tancar sessió', style: TextStyle(fontSize: 16)),
                onTap: () async {
                  // Update the state of the app.
                  SharedPreferences pref = await SharedPreferences.getInstance();
                  await pref.clear();
                  widget.usuari_actual = null;
                  widget.act_token = null;
                  //TODO REMOVE DE SHARED PREFERENCES
                  Navigator.pushAndRemoveUntil(context,
                      MaterialPageRoute(builder: (_) => LogIn()), (route) => false);

                },
              ),
              Container(
                height: 120.0,
                margin: EdgeInsets.only(top: 60),
                decoration: BoxDecoration(
                  border: Border(
                    top: BorderSide(
                      color: Colors.black,
                      width: 3.0,
                    ),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    ListTile(
                      title: Text('Avís legal', style: TextStyle(fontSize: 16)),
                      onTap: () {
                        // Update the state of the app.
                        // ...
                      },
                    ),
                    ListTile(
                      title: Text('Rate us', style: TextStyle(fontSize: 16)),
                      onTap: () {
                        // Update the state of the app.
                        // ...
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CreateEventScreen(usuari_actual: widget.usuari_actual, event_bloc: _eventBloc, usuari_bloc: _usuariBloc, sportDict: sportDict, sportList: sportList, token: widget.act_token),
              ),
            );
          },
          tooltip: 'Increment',
          child: Icon(Icons.add),
        ),
      ),
    );
  }
}

class Search extends SearchDelegate {
  EventBloc event_bloc = null;
  UsuariBloc usuari_bloc = null;
  Usuari usuari_actual = null;
  final List<String> listElem;
  List<String> recentList = [];
  String selectedResult = "";

  Search({this.listElem, this.event_bloc, this.usuari_bloc, this.usuari_actual});

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
        icon: Icon(Icons.close),
        onPressed: () {
          query = "";
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Container(
      child: event_bloc == null ? StreamBuilder<List<Usuari>>(
          stream: usuari_bloc.usuariListStream,
          builder: (BuildContext context, AsyncSnapshot<List<Usuari>> snapshot) {
            return snapshot.data == null ? Container() : new ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (context, index) {
                return snapshot.data[index].nomUsuari != query? Container() : new ManageUsuariList(usuari_llista:
                snapshot.data[index], usuari_bloc: usuari_bloc, usuari_actual: usuari_actual,);
              },
            );
          }
      ) : StreamBuilder<List<Event>>(
          stream:event_bloc.eventListStream,
          builder: (BuildContext context, AsyncSnapshot<List<Event>> snapshot) {
            return snapshot.data == null ? Container() : new ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (context, index) {
                return snapshot.data[index].codi != query? Container() : new ManageEventList(event_llista:
                snapshot.data[index], event_bloc: event_bloc, usuari_actual: usuari_actual, usuari_bloc: usuari_bloc,);
              },
            );
          }
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<String> suggestionList = [];
    query.isEmpty
        ? suggestionList = recentList //In the true case
        : suggestionList.addAll(listElem.where(
      // In the false case
          (element) => element.contains(query),
    ));

    return ListView.builder(
      itemCount: suggestionList.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(
            suggestionList[index],
          ),
          leading: query.isEmpty ? Icon(Icons.access_time) : SizedBox(),
          onTap: (){
            selectedResult = suggestionList[index];
            query = selectedResult;
            showResults(context);
          },
        );
      },
    );
  }
}