import 'dart:io';

import 'package:app_meet_sports/homePage.dart';
import 'package:app_meet_sports/usuariBloc.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'event.dart';
import 'eventSon.dart';
import 'eventBloc.dart';
import 'usuari.dart';
import 'sport.dart';
import 'package:http/http.dart';
import 'package:flutter/services.dart';

class CreateEventScreen extends StatefulWidget {

  final Usuari usuari_actual;
  final EventBloc event_bloc;
  final UsuariBloc usuari_bloc;
  final Map sportDict;
  final List<Sport> sportList;
  final String token;
  CreateEventScreen({Key key, @required this.usuari_actual, this.event_bloc, this.usuari_bloc, this.sportDict, this.sportList, this.token}) : super(key: key);

  @override
  _CreateEventState createState() => _CreateEventState();
}

class _CreateEventState extends State<CreateEventScreen> {

  //Atributs
  var _eventTitleController = TextEditingController();
  var _eventParticipantsController = TextEditingController();
  var _eventSportController = 'Padel';
  var _eventDateController = TextEditingController();
  var _eventTimeController = TextEditingController();
  var _eventCityController = TextEditingController();
  var _eventDirectionController = TextEditingController();
  var _pickperiod = 'No periòdic';

  var _period = false;
  var _eventType = false;

  var _periodicity = ['No periòdic', 'Diàriament', 'Setmanalment', 'Mensualment'];

  Client client;

  //Desplegar calendari per seleccionar la data
  _selectDate(BuildContext context) async {
    var _pickedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime(DateTime.now().year + 1));

    if (_pickedDate != null) {
      setState(() {
        _eventDateController.text = DateFormat('yyyy-MM-dd').format(_pickedDate).toString();
      });
    }
  }

  //Desplegar rellotge per seleccionar l'hora
  _selectTime(BuildContext context) async {

    var _pickedTime = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );

    if (_pickedTime != null) {
      setState(() {
        if (_pickedTime.hour < 10 && _pickedTime.minute < 10) _eventTimeController.text = '0${_pickedTime.hour}:0${_pickedTime.minute}';
        else if (_pickedTime.hour < 10) _eventTimeController.text = '0${_pickedTime.hour}:${_pickedTime.minute}';
        else if(_pickedTime.minute < 10) _eventTimeController.text = '${_pickedTime.hour}:0${_pickedTime.minute}';
        else _eventTimeController.text = '${_pickedTime.hour}:${_pickedTime.minute}';
      });
    }
  }

  //Desplegar mapa per seleccionar ubicació

  //Omplir atributs de la classe event
  Event _event = new Event();
  EventSon _eventSon = new EventSon();

  setAttributes() {
    /*_event.activeSon = _eventSon;
    _eventSon.eventParent = _event;
    _event.activeSon.eventPlace = _eventPlaceController.text;
    _event.activeSon.participantsList.add(widget.usuari_actual.nomUsuari);*/

    _event.title = _eventTitleController.text;
    _event.creator = widget.usuari_actual;
    _event.startDate = _eventDateController.text;
    _event.startTime = _eventTimeController.text;
    _event.city = _eventCityController.text;
    _event.direction = _eventDirectionController.text;
    _event.rating = 0.0;
    _event.eventType = _eventType;
    _event.period = _period;
    _event.isActive = true;

    _event.maxParticipants = int.parse(_eventParticipantsController.text);

    if(_event.period == true) {

      switch(_pickperiod) {
        case "Diàriament":
          _event.periodicity = "DAY";
          _event.repeatAfter = 1;
          break;

        case "Setmanalment":
          _event.periodicity = "WEK";
          _event.repeatAfter = 1;
          break;

        case "Mensualment":
          _event.periodicity = "WEK";
          _event.repeatAfter = 4;
          break;

        default:
          _event.periodicity = null;
          _event.repeatAfter = null;
          break;
      }
    }

    var found = false;
    var i = 0;
    while(!found) {
      if(widget.sportList[i].sportName == _eventSportController) {
        _event.sportType = widget.sportList[i];
        found = true;
      }
      else
        i++;
    }
  }

  //Build
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
          title: Text ('Crear event')
      ),
      body: Padding (
        padding: EdgeInsets.all(14.0),
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Column(
            children: <Widget> [
              TextField(
                controller: _eventTitleController,
                inputFormatters: [
                  LengthLimitingTextInputFormatter(18),
                ],
                decoration: InputDecoration(
                  labelText: "Títol",
                  hintText: "Escriu un títol per l'event.",
                ),
              ),

              TextField(
                controller: _eventCityController,
                //readOnly: true,
                //showCursor: true,
                decoration: InputDecoration(
                  labelText: "Ciutat",
                  hintText: "Determina la ciutat de l'event.",
                ),
              ),

              TextField(
                controller: _eventDirectionController,
                //readOnly: true,
                //showCursor: true,
                decoration: InputDecoration(
                  labelText: "Direcció",
                  hintText: "Determina la direcció de l'event.",
                ),
              ),

              TextField(
                controller: _eventParticipantsController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: "Participants",
                  hintText: "Determina el màxim de participants",
                ),
              ),
              DropdownButtonFormField(
                value: _eventSportController,
                decoration: InputDecoration(
                  labelText: "Esport",
                  hintText: "Selecciona l'esport.",
                ),
                onChanged: (value) {
                  setState(() {
                    _eventSportController = value;
                  });
                },
                items: widget.sportList.map((data) {
                  return DropdownMenuItem<String>(
                    child: Text(data.sportName),
                    value: data.sportName,
                  );
                }).toList(),
              ),

              TextField(
                controller: _eventDateController,
                readOnly: true,
                showCursor: true,
                decoration: InputDecoration(
                  labelText: "Data d'inici",
                  hintText: "Selecciona la data d'inici de l'event.",
                  suffixIcon: InkWell(
                    onTap: () {
                      _selectDate(context);
                      FocusScope.of(context).unfocus();
                    },
                    child: Icon(Icons.calendar_today_rounded),
                  ),
                ),
              ),
              TextField(
                controller: _eventTimeController,
                readOnly: true,
                showCursor: true,
                decoration: InputDecoration(
                  labelText: "Hora d'inici",
                  hintText: "Selecciona l'hora d'inici de l'event.",
                  suffixIcon: InkWell(
                    onTap: () {
                      _selectTime(context);
                      FocusScope.of(context).unfocus();
                    },
                    child: Icon(Icons.access_alarm_rounded),
                  ),
                ),
              ),

              DropdownButtonFormField(
                value: _pickperiod,
                decoration: InputDecoration(
                  labelText: "Periodicitat",
                  hintText: "En cas de event periòdic, selecciona el període de repetició.",
                ),
                items: _periodicity.map((data) {
                  return DropdownMenuItem<String>(
                    child: Text(data),
                    value: data,
                  );
                }).toList(),
                onChanged: (value) {
                  setState(() {
                    _pickperiod = value;
                  });
                  },
              ),

              SizedBox(
                height: 15,
              ),

              Row (
                  children: <Widget> [
                    Checkbox(
                      value: _eventType,
                      onChanged: (newValue) {
                        setState(() {
                          _eventType = newValue;
                        });
                      }
                  ),

                    Text(
                      'Event competitiu',
                      style: TextStyle(fontSize: 16),
                    ),

                    SizedBox(width: 25),

                    Checkbox(
                        value: _period,
                        onChanged: (newValue) {
                          setState(() {
                            _period = newValue;
                          });
                        }
                    ),

                    Text(
                      'Event periòdic',
                      style: TextStyle(fontSize: 16),
                    ),
                  ]
              ),

              Center (
                    child: RaisedButton(
                      child: Text('Crear'),
                      onPressed: () {
                        setAttributes();
                        widget.event_bloc.createEvent(_event, _eventSon);
                        sleep(Duration(seconds: 2));
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => HomePage(usuari_actual: widget.usuari_actual, act_token: widget.token,),
                            ),
                        );
                     },
                  ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}