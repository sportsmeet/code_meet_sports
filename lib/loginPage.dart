import 'dart:convert';
import 'package:app_meet_sports/usuari.dart';
import 'package:flutter/material.dart';
import 'package:app_meet_sports/registerPage.dart';
import 'package:http/http.dart' as http;
import 'homePage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LogIn extends StatefulWidget {
  @override
  _LogInState createState() => new _LogInState();
}

class _LogInState extends State<LogIn> {
  final _formKey = GlobalKey<FormState>();
  String my_token="";
  TextEditingController _username = TextEditingController();
  TextEditingController _password = TextEditingController();
  @override
  Widget build(BuildContext context) {

    return Form(
      key: _formKey,
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 60.0),
                child: Center(
                  child: Container(
                      width: 200,
                      height: 150,
                      child: Image.asset('assets/imatges/meetSports.png')),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20,vertical: 5),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(30),
                ),
                child: TextField (
                  controller: _username,
                  decoration: InputDecoration(
                    icon: Icon(Icons.person),
                    hintText: "Usuari",

                  ),
                ),
              ),

              Container(
                padding: EdgeInsets.symmetric(horizontal: 20,vertical: 5),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(30),
                ),
                child: TextField (
                  controller: _password,
                  obscureText: true,
                  decoration: InputDecoration(
                    icon: Icon(Icons.lock),
                    hintText: "Contrasenya",
                  ),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,

                child: FlatButton(
                  padding: EdgeInsets.fromLTRB(25, 20, 100, 50),
                  onPressed: (){
                    //TODO IMPLEMENTAR
                  },
                  child: Text(
                    'Has oblidat la contrasenya?',
                    style: TextStyle(color: Colors.blue, fontSize: 15),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 50,vertical: 5),
                child: Row(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(right: 10.0),
                      height: 40,
                      width: 140,
                      decoration: BoxDecoration(
                          color: Colors.blue, borderRadius: BorderRadius.circular(20)),
                      child: FlatButton(
                        onPressed: () async {
                          Map<String,String> data = {
                            "username": _username.text,
                            "password": _password.text,
                          };
                          //var response = await http.post(Uri.parse("${_baseUrl}user/", data);
                          final response= await http.post(
                              Uri.parse('http://10.0.2.2:8000/api-token-auth/'),
                              body: {'username' : data["username"],
                                     'password' : data["password"]},
                          );
                          if(response.statusCode == 200 || response.statusCode == 201){
                            Map<String,dynamic> output = json.decode(response.body);
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Inici de sessió correcte!")));
                            my_token=output["token"];
                            setPreferences();
                            var headers = {
                              'Content-Type': 'application/x-www-form-urlencoded',
                              'Authorization' : 'token ' + my_token,
                            };
                            var request = http.Request('GET', Uri.parse('http://10.0.2.2:8000/user'));
                            request.headers.addAll(headers);
                            http.StreamedResponse response2 = await request.send();
                            final resposta = await response2.stream.bytesToString();
                            Usuari user_act = Usuari.fromJson(json.decode(resposta));
                            //TODO AQUI JA GUARDARIA EL TOKEN A SHARED PREFERENCES
                            Navigator.pushAndRemoveUntil(context,
                                MaterialPageRoute(builder: (_) => HomePage(usuari_actual: user_act, act_token: my_token,)), (route) => false);

                          }else{
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Usuari o contrasenya incorrectes")));
                          }
                        },
                        child: Text(
                          'Iniciar sessió',
                          style: TextStyle(color: Colors.white, fontSize: 15),
                        ),
                      ),
                    ),
                    Container(
                      height: 40,
                      width: 140,
                      decoration: BoxDecoration(
                          color: Colors.blue, borderRadius: BorderRadius.circular(20)),
                      child: FlatButton(
                        onPressed: () {
                          Navigator.push(
                              context, MaterialPageRoute(builder: (_) => Register())
                          );
                        },
                        child: Text(
                          'Registrar-se',
                          style: TextStyle(color: Colors.white, fontSize: 15),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void setPreferences() async {
    final pref = await SharedPreferences.getInstance();
    setState(() {
      pref.setString("token", my_token);
    });
  }
  /*void _handleLogIn() async {
    final FormState form = _formKey.currentState;
    if (!form.validate()) {
      showInSnackBar('Please fix the errors in red before submitting.');
    } else {
      form.save();
      _userResponse = await authenticateUser(_username, _password);
      if ((_apiResponse.ApiError as ApiError) == null) {
        _saveAndRedirectToHome();
      } else {
        showInSnackBar((_apiResponse.ApiError as ApiError).error);
      }
    }
  }*/
}

