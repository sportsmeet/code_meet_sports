import 'usuari.dart';
import 'usuariBloc.dart';
import 'eventBloc.dart';
import 'event.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'manageLists.dart';
import 'package:app_meet_sports/usuari.dart';
import 'sport.dart';

class MyEvents extends StatefulWidget {
  Usuari perfil_actual;
  UsuariBloc usuari_bloc;
  EventBloc event_bloc;
  List<Sport> sportList;
  Map<int, String> sportDict;
  final String act_token;
  final user_list;

  MyEvents({Key key, @required this.perfil_actual, this.usuari_bloc, this.event_bloc,
    this.sportDict, this.sportList, this.act_token, this.user_list}) : super(key: key);

  @override
  _MyEventsState createState() => _MyEventsState();
}

class _MyEventsState extends State<MyEvents> {
  String EstatDropdownVal = 'En curs';
  String TipusDropdownVal = 'Tots';
  Map<String, String> filtres = {};

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    filtres['estat'] = "En curs";
    filtres['tipus'] = "Tots";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Els meus events"),
      ),
      body: Container(
        child:
          StreamBuilder<List<Event>>(
            stream: widget.event_bloc.myEventsStream,
            builder: (BuildContext context, AsyncSnapshot<List<Event>> snapshot) {
              return snapshot.data == null ? Center(child: CircularProgressIndicator()) : Container(
                  child:
                  Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 10.0),
                        child:
                        Row(
                          children: [
                            Text("Estat: "),
                            SizedBox(width: 5),
                            DropdownButton<String>(
                              value: EstatDropdownVal,
                              icon: const Icon(Icons.arrow_downward),
                              iconSize: 24,
                              elevation: 16,
                              style: const TextStyle(color: Colors.deepPurple),
                              underline: Container(
                                height: 2,
                                color: Colors.deepPurpleAccent,
                              ),
                              onChanged: (newValue) {
                                setState(() {
                                  if(newValue is String) {
                                    EstatDropdownVal = newValue;
                                    filtres['estat'] = EstatDropdownVal;
                                    widget.event_bloc.actualitzarMyFiltres(filtres, widget.perfil_actual);
                                  }
                                });
                              },
                              items: <String>['En curs', 'Finalitzats'].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            ),
                            SizedBox(width: 20),
                            Text("Tipus: "),
                            SizedBox(width: 5),
                            DropdownButton<String>(
                              value: TipusDropdownVal,
                              icon: const Icon(Icons.arrow_downward),
                              iconSize: 24,
                              elevation: 16,
                              style: const TextStyle(color: Colors.deepPurple),
                              underline: Container(
                                height: 2,
                                color: Colors.deepPurpleAccent,
                              ),
                              onChanged: (newValue) {
                                setState(() {
                                  if(newValue is String) {
                                    TipusDropdownVal = newValue;
                                    filtres['tipus'] = TipusDropdownVal;
                                    widget.event_bloc.actualitzarMyFiltres(filtres, widget.perfil_actual);
                                  }
                                });
                              },
                              items: <String>['Tots', 'Creats', 'Inscrits'].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                          child:
                            ListView.builder(
                            itemCount: snapshot.data.length,
                            itemBuilder: (context, index) {
                              if(snapshot.data[index].activeSon != null) {
                                return new ManageEventList(event_llista: snapshot.data[index], event_bloc: widget.event_bloc,
                                    usuari_actual: widget.perfil_actual, sportDict: widget.sportDict, sportList: widget.sportList,
                                    token: widget.act_token, usuariList: widget.user_list, usuari_bloc: widget.usuari_bloc);
                              } else {
                                return new ManageOldEventList(event_llista: snapshot.data[index], event_bloc: widget.event_bloc,
                                    usuari_actual: widget.perfil_actual, sportDict: widget.sportDict, sportList: widget.sportList,
                                    token: widget.act_token, usuariList: widget.user_list, usuari_bloc: widget.usuari_bloc);
                              }
                            },
                          )
                      ),
                    ],
                  )
              );
            }
        ),
      ),
    );
  }
}