import 'package:app_meet_sports/eventBloc.dart';
import 'package:app_meet_sports/event.dart';
import 'package:app_meet_sports/sport.dart';
import 'package:app_meet_sports/usuari.dart';
import 'package:app_meet_sports/usuariBloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'detallsPage.dart';

class RatingScreen extends StatefulWidget {

  final Event detallEvent;
  final EventBloc eventBloc;
  final Usuari usuariActual;
  final UsuariBloc usuariBloc;
  final Map<int, String> sportDict;
  final List<Sport> sportList;
  final List<Usuari> usuariList;
  final String token;

  RatingScreen({Key key, @required this.detallEvent, this.eventBloc, this.usuariActual, this.usuariBloc,
    this.usuariList, this.sportDict, this.sportList, this.token}) : super(key: key);

  @override
  _RatingScreenState createState() => _RatingScreenState();
}

class _RatingScreenState extends State<RatingScreen> {

  var _eventRatingController = TextEditingController();

  @override
  void initState() {
    if (widget.detallEvent.inactiveSon.participantsIDRating[widget.usuariActual.id] == null)
      _eventRatingController.text = "0.0";
    else
      _eventRatingController.text = widget.detallEvent.inactiveSon.participantsIDRating[widget.usuariActual.id].toString();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text ('Puntuar event')
      ),
      body: Padding (
        padding: EdgeInsets.all(14.0),
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Column(
            children: [
              SizedBox(height: 150),
              Container (
                child: widget.detallEvent.inactiveSon.participantsIDRating.containsKey(widget.usuariActual.id) &&
                    widget.detallEvent.inactiveSon.participantsIDRating[widget.usuariActual.id] != null ?
                    Column(
                      children: [
                        Text(
                          "Actualitza la teva puntuació per l'event",
                          style: TextStyle(fontSize: 16),
                        ),
                        Text(
                          "Puntuació anterior:",
                          style: TextStyle(fontSize: 16),
                        ),
                      ],
                    ) :
                Text(
                  "Introdueix la teva puntuació per l'event",
                  style: TextStyle(fontSize: 16),
                ),
              ),
              SizedBox(height: 20),
              RatingBarIndicator(
                rating: widget.detallEvent.inactiveSon.participantsIDRating.containsKey(widget.usuariActual.id) &&
                    widget.detallEvent.inactiveSon.participantsIDRating[widget.usuariActual.id] != null ?
                        widget.detallEvent.inactiveSon.participantsIDRating[widget.usuariActual.id] :
                        widget.detallEvent.rating,
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                itemCount: 5,
                itemSize: 35,
                direction: Axis.horizontal,
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: TextFormField (
                  controller: _eventRatingController,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Puntuació",
                    hintText: "Puntua l'event",
                    suffixIcon: MaterialButton(
                      onPressed: () {
                        setState(() {
                          widget.detallEvent.inactiveSon.participantsIDRating[widget.usuariActual.id] = double.parse(_eventRatingController.text);
                        });
                        widget.eventBloc.puntuarEvent(widget.detallEvent, widget.usuariActual, double.parse(_eventRatingController.text));
                        Navigator.pop(
                          context,
                        );
                      },
                      child: Text("Puntua")
                    )
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}