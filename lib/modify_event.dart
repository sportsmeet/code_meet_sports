import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'usuari.dart';
import 'sport.dart';
import 'event.dart';
import 'eventBloc.dart';
import 'homePage.dart';

class ModifyEventScreen extends StatefulWidget {

  final Event eventModified;
  final EventBloc eventBloc;
  final Usuari usuariActual;
  final Map<int, String> sportDict;
  final List<Sport> sportList;
  final String token;

  ModifyEventScreen({Key key, @required this.eventModified, this.eventBloc, this.usuariActual, this.sportDict, this.sportList, this.token}) : super(key: key);

  @override
  _ModifyEventState createState() => _ModifyEventState();
}

class _ModifyEventState extends State<ModifyEventScreen> {

  //Atributs
  var _eventTitleController = TextEditingController();
  var _eventDescriptionController = TextEditingController();
  var _eventDateController = TextEditingController();
  var _eventTimeController = TextEditingController();
  var _eventCityController = TextEditingController();
  var _eventDirectionController = TextEditingController();
  var _eventSportController;

  //Desplegar calendario para seleccionar la fecha
  _selectDate(BuildContext context) async {
    var _pickedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime(DateTime.now().year + 1));

    if (_pickedDate != null) {
      setState(() {
        _eventDateController.text = DateFormat('yyyy-MM-dd').format(_pickedDate).toString();
        widget.eventModified.startDate = _eventDateController.text;
      });
    }
  }

  //Desplegar reloj para seleccionar la hora
  _selectTime(BuildContext context) async {

    var _pickedTime = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );

    if (_pickedTime != null) {
      setState(() {
        if (_pickedTime.hour < 10 && _pickedTime.minute < 10) _eventTimeController.text = '0${_pickedTime.hour}:0${_pickedTime.minute}';
        else if (_pickedTime.hour < 10) _eventTimeController.text = '0${_pickedTime.hour}:${_pickedTime.minute}';
        else if(_pickedTime.minute < 10) _eventTimeController.text = '${_pickedTime.hour}:0${_pickedTime.minute}';
        else _eventTimeController.text = '${_pickedTime.hour}:${_pickedTime.minute}';

        widget.eventModified.startTime = _eventTimeController.text;
      });
    }
  }

  //Desplegar mapa para seleccionar ubicación

  //Build
  @override
  void initState() {
    _eventTitleController.text = widget.eventModified.title;
    _eventCityController.text =  widget.eventModified.city;
    _eventDirectionController.text = widget.eventModified.direction;
    _eventDateController.text = widget.eventModified.startDate;
    _eventTimeController.text = widget.eventModified.startTime;
    _eventSportController = widget.eventModified.sportType.sportName;
  }
  @override
  Widget build(BuildContext context) {

    //Scaffold
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
          title: Text ('Modificar event')
      ),
      body: Padding (
        padding: EdgeInsets.all(14.0),
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Column(
            children: <Widget> [
              TextField(
                controller: _eventTitleController,
                decoration: InputDecoration(
                  labelText: "Títol",
                  hintText: "Escriu una títol per l'event.",
                ),
                onChanged: (text) => widget.eventModified.title = _eventTitleController.text,
              ),

              DropdownButtonFormField(
                value: _eventSportController,
                decoration: InputDecoration(
                  labelText: "Esport",
                  hintText: "Selecciona l'esport.",
                ),
                onChanged: (value) {
                  setState(() {
                    _eventSportController = value;
                  });
                },
                items: widget.sportList.map((data) {
                  return DropdownMenuItem<String>(
                    child: Text(data.sportName),
                    value: data.sportName,
                  );
                }).toList(),
              ),

              TextField(
                controller: _eventDateController,
                readOnly: true,
                showCursor: true,
                decoration: InputDecoration(
                  labelText: "Data d'inici",
                  hintText: "Selecciona la data d'inici de l'event.",
                  suffixIcon: InkWell(
                    onTap: () {
                      _selectDate(context);
                      FocusScope.of(context).unfocus();
                    },
                    child: Icon(Icons.calendar_today_rounded),
                  ),
                ),
                onChanged: (text) => widget.eventModified.startDate = _eventDateController.text,
              ),

              TextField(
                controller: _eventTimeController,
                readOnly: true,
                showCursor: true,
                decoration: InputDecoration(
                  labelText: "Hora d'inici",
                  hintText: "Selecciona l'hora d'inici de l'event.",
                  suffixIcon: InkWell(
                    onTap: () {
                      _selectTime(context);
                      FocusScope.of(context).unfocus();
                    },
                    child: Icon(Icons.access_alarm_rounded),
                  ),
                ),
                onChanged: (text) => widget.eventModified.startTime = _eventTimeController.text,
              ),

              TextField(
                controller: _eventCityController,
                //readOnly: true,
                //showCursor: true,
                decoration: InputDecoration(
                  labelText: "Ciutat",
                  hintText: "Determina la ciutat de l'event.",
                ),
                onChanged: (text) => widget.eventModified.city = _eventCityController.text,
              ),

              TextField(
                controller: _eventDirectionController,
                //readOnly: true,
                //showCursor: true,
                decoration: InputDecoration(
                  labelText: "Direcció",
                  hintText: "Determina la direcció de l'event.",
                ),
                onChanged: (text) => widget.eventModified.direction = _eventDirectionController.text,
              ),

              SizedBox(
                height: 15,
              ),

              Row (
                  children: <Widget> [
                    Checkbox(
                        value: widget.eventModified.eventType,
                        onChanged: (newValue) {
                          setState(() {
                            widget.eventModified.eventType = newValue;
                          });
                        }
                    ),

                    Text(
                      'Event competitiu',
                      style: TextStyle(fontSize: 16),
                    ),

                    SizedBox(width: 25),
                    widget.eventModified.period  ?
                      Checkbox(
                          value: widget.eventModified.isActive,
                          onChanged: (newValue) {
                            setState(() {
                              widget.eventModified.isActive = newValue;
                            });
                          }
                      ) : Container(),
                    widget.eventModified.period  ?
                      Text(
                        'Periodicitat',
                        style: TextStyle(fontSize: 16),
                      ) : Container(),
                  ]
              ),
              RaisedButton (
                child: Text('Modificar'),
                onPressed: () {
                  if(widget.eventModified.isActive == false)
                    widget.eventModified.period = false;

                  var found = false;
                  var i = 0;
                  while(!found) {
                    if(widget.sportList[i].sportName == _eventSportController) {
                      widget.eventModified.sportType = widget.sportList[i];
                      found = true;
                    }
                    else
                      i++;
                  }
                  widget.eventBloc.modifyEvent(widget.eventModified);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => HomePage(usuari_actual: widget.usuariActual, act_token: widget.token),
                    ),
                  );
                },
              ) ,
            ],
          ),
        ),
      ),
    );
  }
}